const express = require('express');
const isEmpty = require('locutus/php/var/empty');
const HttpStatus = require('http-status-codes');
const ResponseHelper = require('../helpers/response.helper');

/**
 * Return posts router
 *
 * @param {DatabaseHelper} db
 */
module.exports = (db) => {
    const router = express.Router();

    router.get('/', (req, res, next) => {
        const limit = isEmpty(req.query.limit) ? 10 : req.query.limit;
        const offset = isEmpty(req.query.offset) ? 0 : req.query.offset;

        ResponseHelper.modelResponse(res, db.getPosts(limit, offset));
    });

    router.get('/count', (req, res, next) => {
        ResponseHelper.modelResponse(res, {
            totalPostsCount: db.getPostsCount(),
        });
    });

    router.get('/single', (req, res, next) => {
        if (isEmpty(req.query.id)) {
            next(ResponseHelper.createError(HttpStatus.BAD_REQUEST, 'ID missing from query string'));
            return;
        }

        const record = db.getPost(req.query.id);

        if (isEmpty(record)) {
            next(ResponseHelper.createError(HttpStatus.NOT_FOUND));
            return;
        }

        ResponseHelper.modelResponse(res, record);
    });

    router.post('/', (req, res, next) => {
        if (isEmpty(req.body) || isEmpty(req.body.title) || isEmpty(req.body.content)) {
            next(ResponseHelper.createError(HttpStatus.BAD_REQUEST, 'Invalid request body'));
            return;
        }

        const result = db.addPost(req.body.title, req.body.content);

        ResponseHelper.modelResponse(res, {
            recordsAdded: result,
        });
    });

    router.use((err, req, res, next) => {
        console.error(err);
        ResponseHelper.errorResponse(res, err);
    });

    return router;
};
