const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const HttpStatus = require('http-status-codes');

const DatabaseHelper = require('./helpers/database.helper');
const ResponseHelper = require('./helpers/response.helper');
const postsRouter = require('./routes/posts');

const app = express();
const dbConnection = new DatabaseHelper();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/posts', postsRouter(dbConnection));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/public/index.html'));
});

// error handler
app.use((err, req, res, next) => {
    console.error(err);
    ResponseHelper.errorResponse(res, err);
});

process.on('exit', () => dbConnection.closeDbConnection());
process.on('SIGHUP', () => process.exit(128 + 1));
process.on('SIGINT', () => process.exit(128 + 2));
process.on('SIGTERM', () => process.exit(128 + 15));

module.exports = app;
