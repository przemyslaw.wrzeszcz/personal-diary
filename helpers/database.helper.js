const path = require('path');
const SqLite = require('better-sqlite3');
const moment = require('moment');

class DatabaseHelper {
    constructor() {
        this.db = new SqLite(path.join(__dirname, './../blogDB.sqlite'), { verbose: console.log });
        this.postsTableName = 'db_posts';
    }

    closeDbConnection() {
        this.db.close();
    }

    getPostsCount() {
        const result = this.db.prepare(`SELECT COUNT(*) as 'total' FROM ${this.postsTableName}`).get();

        return result.total;
    }

    getPosts(limit = 10, offset = 0) {
        return this.db.prepare(`SELECT * FROM ${this.postsTableName} ORDER BY create_date ASC LIMIT ${offset},${limit}`).all();
    }

    getPost(id) {
        return this.db.prepare(`SELECT * FROM ${this.postsTableName} WHERE id = ?`).get(id);
    }

    addPost(title, content) {
        const stmt = this.db.prepare(`INSERT INTO ${this.postsTableName} (title, content, update_date, create_date) VALUES (?, ?, ?, ?)`);
        const currentDate = moment().toISOString();
        const info = stmt.run(title, content, currentDate, currentDate);

        return info.changes;
    }

    editPost(id, title, content) {
        const stmt = this.db.prepare(`UPDATE ${this.postsTableName} SET title = ?, content = ?, update_date = ? WHERE id = ?`);
        const currentDate = moment().toISOString();
        const info = stmt.run(title, content, currentDate, id);

        return info.changes;
    }
}

module.exports = DatabaseHelper;
