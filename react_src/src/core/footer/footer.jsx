import React from 'react';
import {Link} from 'react-router-dom';
import Logo from './img/ftr-logo.png';

import {
    Navbar,
    Nav,
    NavItem,
} from 'reactstrap';

const CompFooter = ({position}) => {
    return (
        <footer>
            <Navbar color="light" light expand="md">
                <img src={`/${Logo}`} alt=""/>
                <Nav className="ml-auto">
                    <p className="copyright">&copy;Mulczer24 2018</p>
                </Nav>
            </Navbar>
        </footer>
    );
};

export default CompFooter;