import React from 'react';
import {Link, NavLink, Redirect} from 'react-router-dom';
import logo from './img/logo.png';
import FontAwesome from 'react-fontawesome';
import axios from 'axios';
import settings from '../settings';
import { MyContext } from "../../context";

import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
    Container,
    DropdownToggle,
    DropdownItem,
    DropdownMenu,
    UncontrolledDropdown,
} from 'reactstrap';

class CompHeader extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.signOut = this.signOut.bind(this);
        this.state = {
            isOpen: false,
            refPassed: false,
            isSignedOut: false,
        };
        this.myRef = React.createRef();
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    componentDidMount(){
        if (typeof this.myRef.current !== 'undefined' && this.state.refPassed === false) {
            this.setState({
                refPassed: true,
            });
            this.props.setHeadRef(this.myRef.current);
        }
    }

    componentDidUpdate(){
        if (typeof this.myRef.current !== 'undefined' && this.state.refPassed === false) {
            this.setState({
                refPassed: true,
            });
            this.props.setHeadRef(this.myRef.current);
        }
    }
    signOut(e){
        e.stopPropagation();
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
        e.nativeEvent.preventDefault();
        if (this.props.userToken) {
            new Promise((resolve) => {
                axios.delete(`${settings.backendDomain}/users`, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                    .then(()=> {
                        resolve();
                    })
                    .catch(() =>{
                        resolve();
                    });
            }).then(() => {
                localStorage.removeItem('abvReactCache');
                // this.props.setToken('');
                setTimeout(() => {
                    // window.location.reload();
                    window.location.assign(settings.frontendDomain)
                }, 50);
            });
        }
    }
    render() {
        return (
            <header ref={this.myRef}>
                <Navbar color="light" light expand="md">
                    <Container>
                        <Link to="/" className="navbar-brand">
                            <img src={`/${logo}`} alt="Logo Mulczer 24"/>
                        </Link>
                        <NavbarToggler onClick={this.toggle}/>
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                {this.props.userToken ?
                                    <NavItem>
                                        <span className="nav-link">Witaj, {this.props.userData.name}</span>
                                    </NavItem>: null
                                }
                                {(this.props.userData.role === 'customer') ?
                                    <NavItem>
                                        <NavLink exact={true} to="/" activeClassName="active" className="nav-link">Wystawione zamówienia</NavLink>
                                    </NavItem>: null
                                }
                                {this.props.userToken ?
                                    <NavItem>
                                        <UncontrolledDropdown nav inNavbar tag="ul" className="remove-left-padding">
                                            <DropdownToggle className="nav-link">
                                                <FontAwesome name='user-circle'/>Moje konto
                                            </DropdownToggle>
                                            <DropdownMenu right tag="ul">
                                                <NavItem>
                                                    <NavLink exact={true} to="/moje-konto" activeClassName="active" className="nav-link">Moje dane</NavLink>
                                                </NavItem>
                                                {(this.props.userData.role === 'executor') ?
                                                    <React.Fragment>
                                                        <NavItem>
                                                            <NavLink exact={true} to="/moje-konto/obserwowane" activeClassName="active" className="nav-link">Oserwowane</NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink exact={true} to="/moje-konto/moje-oferty" activeClassName="active" className="nav-link">Złożone oferty</NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink exact={true} to="/moje-konto/moje-oferty/wygrane" activeClassName="active" className="nav-link">Zwycięskie oferty</NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink exact={true} to="/moje-konto/dane-do-faktury" activeClassName="active" className="nav-link">Dane do faktury</NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink exact={true} to="/moje-konto/dane-wykonawcy" activeClassName="active" className="nav-link">Dane wykonawcy</NavLink>
                                                        </NavItem>
                                                    </React.Fragment>:
                                                    <React.Fragment>
                                                        <NavItem>
                                                            <NavLink exact={true} to="/moje-konto/zamowienia" activeClassName="active" className="nav-link">Moje zamówienia</NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink exact={true} to="/moje-konto/dane-do-umowy" activeClassName="active" className="nav-link">Dane do umowy</NavLink>
                                                        </NavItem>
                                                        <Link className="btn btn-tertiary btn-block" exact="true" to="/moje-konto/dodaj-zamowienie">Dodaj zamówienie</Link>
                                                    </React.Fragment>
                                                }
                                            </DropdownMenu>
                                        </UncontrolledDropdown>
                                    </NavItem>
                                    : null
                                }
                                {this.props.userToken ?
                                    <NavItem>
                                        <a href="#" onClick={this.signOut} className="nav-link"><FontAwesome name='sign-out-alt'/>Wyloguj się</a>
                                    </NavItem>:
                                    <NavItem>
                                        <NavLink to="/logowanie" activeClassName="active" className="nav-link"><FontAwesome name='sign-in-alt'/>Zaloguj się</NavLink>
                                    </NavItem>
                                }
                            </Nav>
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({setHeadRef, setToken, state}) => {
            return <CompHeader {...props} setHeadRef={setHeadRef} setToken={setToken} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)
