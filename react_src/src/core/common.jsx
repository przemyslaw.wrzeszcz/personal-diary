import locutusMath from "locutus/php/math";
import moment from 'moment';

export default class GWCommon {
    static formatDate(date) {
        const mm = date.getMonth() + 1; // getMonth() is zero-based
        const dd = date.getDate();
        const hh = date.getHours();
        const min = date.getMinutes();
        const sec = date.getSeconds();

        return [date.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd,
        ].join('-') + ' ' + [
            (hh>9 ? '' : '0') + hh,
            (min>9 ? '' : '0') + min,
            (sec>9 ? '' : '0') + sec,
        ].join(':');
    }

    static gcd(a, b) {
        return (a % b) ? GWCommon.gcd(b, a%b) : b;
    }

    static getAspectRatio(w, h) {
        const div = GWCommon.gcd(w, h);

        return {
            w: parseInt(locutusMath.round(w/div, 0)),
            h: parseInt(locutusMath.round(h/div, 0)),
        }
    }

    static makeid() {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    static declinationPL(inputNo, singular, mnoga, mnoga_dopelniacz) {
        const no = Math.abs(inputNo);
        if (no === 1) return singular;
        const divided10 = no % 10;
        const divided100 = no % 100;
        if (divided10 > 4 || divided10 < 2 || (divided100 < 15 && divided100 > 11))
            return mnoga_dopelniacz;
        return mnoga;
    }

    static escapeDiacritics(str) {
        return str.replace(/ą/g, 'a').replace(/Ą/g, 'A')
            .replace(/ć/g, 'c').replace(/Ć/g, 'C')
            .replace(/ę/g, 'e').replace(/Ę/g, 'E')
            .replace(/ł/g, 'l').replace(/Ł/g, 'L')
            .replace(/ń/g, 'n').replace(/Ń/g, 'N')
            .replace(/ó/g, 'o').replace(/Ó/g, 'O')
            .replace(/ś/g, 's').replace(/Ś/g, 'S')
            .replace(/ż/g, 'z').replace(/Ż/g, 'Z')
            .replace(/ź/g, 'z').replace(/Ź/g, 'Z');
    }

    static calcOfferDeadline(startDate) {
        const start = moment(startDate);

        return start.subtract(3, "days").format("YYYY-MM-DD");
    }

    static calcDateDiffInDays(endDate, startDate = {}) {
        const start = moment(startDate);
        const end = moment(endDate);

        return Math.round(moment.duration(end.diff(start)).asDays());
    }

    static IsSafariBrowser() {
        const VendorName=window.navigator.vendor;

        return ((VendorName.indexOf('Apple') > -1) && (window.navigator.userAgent.indexOf('Safari') > -1));
    }

    static validatenip(nip) {
        const nipWithoutDashes = nip.replace(/-/g,"");
        const reg = /^[0-9]{10}$/;

        if(reg.test(nipWithoutDashes) === false) {
            return false;
        } else {
            const digits = (""+nipWithoutDashes).split("");
            const checksum = (6*parseInt(digits[0]) + 5*parseInt(digits[1]) + 7*parseInt(digits[2]) + 2*parseInt(digits[3]) + 3*parseInt(digits[4]) + 4*parseInt(digits[5]) + 5*parseInt(digits[6]) + 6*parseInt(digits[7]) + 7*parseInt(digits[8]))%11;

            return (parseInt(digits[9]) === checksum);
        }
    }
}
