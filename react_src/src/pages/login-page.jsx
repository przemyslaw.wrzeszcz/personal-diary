import React from 'react';
import {Container, Row, Col } from 'reactstrap';
import LoginForm from '../modules/loginForm/login-form.jsx'
import BackgroundImage from '../modules/registerForm/img/register-bg.jpg';


const LoginPage = () => (
    <main>
        <Container fluid>
            <img className="background-image" src={`/${BackgroundImage}`} alt=""/>
            <Row>
                <Col>
                    <LoginForm />
                </Col>
            </Row>

        </Container>
    </main>
);

export default LoginPage;