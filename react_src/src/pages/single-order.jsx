import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import LeafletMap from '../modules/leaflet-map/map';
import OfferWindow from '../modules/offer-window/offer';
import OfferInfo from '../modules/offer-info/offer';
import NearOffers from '../modules/near-offers/offer';
import Video from '../modules/video/video';
import OrderCustomerDetails from '../modules/order-client-details';
import GallerySection from '../modules/gallery';
import OrderExtraFiles from '../modules/order-extra-files';
import {Container, Row, Col, Alert} from 'reactstrap';
import FollowButton from '../modules/user-followed';
import axios from 'axios';
import { MyContext } from "../context";
import Preloader from "../modules/preloader/preloader";
import TimeLeft from "../modules/list-group/time-left";
import settings from "../core/settings";
import GWCommon from "../core/common";
import ErrorPage from "./errorsite";

class SingleOrder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orderData: {},
            requestSend: false,
            errorData: false,
            mainPhoto: {},
            photoIndex: 0,
            orderId: 0,
            watchedByMe: false,
            nearby: false,
            myOffer: false,
        };
        this.getData = this.getData.bind(this);
        this.abortRequest = this.abortRequest.bind(this);
        this.updateWatched = this.updateWatched.bind(this);
        this.updateMyOffer = this.updateMyOffer.bind(this);
        this.selectWinningOffer = this.selectWinningOffer.bind(this);
        this.goBack = this.goBack.bind(this);
    }

    getData(id) {
        if (this.props.userToken.length > 0) {
            const instance = axios.create({
                headers: {'Authorization': `Bearer ${this.props.userToken}`},
            });
            this.serverRequest =
                instance
                    .get(`${settings.backendDomain}/orders/${id}`, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                    .then((result) => {
                        const newState = {
                            errorData: false,
                            requestSend: true,
                            orderData: result.data,
                            mainPhoto: result.data.mainPhoto,
                            photoIndex: result.data.photoIndex,
                        };

                        if (result.data.hasOwnProperty('watchedByMe')) {
                            newState.watchedByMe = result.data.watchedByMe;
                        }

                        if (result.data.hasOwnProperty('nearby')) {
                            newState.nearby = result.data.nearby;
                        }

                        if (result.data.hasOwnProperty('myOffer')) {
                            newState.myOffer = result.data.myOffer;
                        }

                        if (result.data.hasOwnProperty('offers')) {
                            newState.offers = result.data.offers;
                        }

                        this.setState(newState);
                    }).catch((e) => {
                        this.setState({
                            requestSend: true,
                            errorData: true,
                        });
                    });
        }
    }

    updateMyOffer(value) {
        this.setState({
            myOffer: value,
        });
    }

    selectWinningOffer(winId) {
        const orderData = this.state.orderData;
        orderData.order.closed = 1;
        const orderOffers = [];
        this.state.offers.map(val => {
            if (val.id === winId) {
                val.selected = "1";
            }

            orderOffers.push(val);
        });

        this.setState({
            orderData: orderData,
            offers: orderOffers,
        });
    }

    updateWatched(value) {
        this.setState({
            watchedByMe: value,
        });
    }

    abortRequest() {
        if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
            this.serverRequest.abort();
        }
    }

    componentWillUnmount() {
        this.abortRequest();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match.params.id !== this.props.match.params.id) {
            this.abortRequest();
            this.setState({
                requestSend: false,
            });
            this.getData(nextProps.match.params.id);
        }
    }

    componentDidMount() {
        this.getData(this.props.match.params.id);
    }

    goBack(e) {
        e.stopPropagation();
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
        e.nativeEvent.preventDefault();

        this.props.history.goBack();
    }

    render() {
        if (this.props.userToken.length === 0) {
            return <Redirect to="/logowanie"/>
        }

        if (this.state.errorData) {
            return (
                <ErrorPage />
            );
        }

        if (!this.state.requestSend) {
            return (
                <Preloader />
            );
        }

        const orderData = this.state.orderData;

        return (
            <main>
                <Container>
                    <Row>
                        <Col xs="8" className="offer-info-wrap">
                            {
                                (this.props.userData.role === 'executor') ?
                                    <React.Fragment>
                                        <a href="#" onClick={this.goBack}>&lt; powrót</a>
                                        {/*<Link to="/">&lt; powrót do listy zamówień</Link>*/}
                                        <FollowButton button orderId={orderData.order.id} watchedByMe={this.state.watchedByMe} updateWatched={this.updateWatched} />
                                    </React.Fragment>
                                    :
                                    <React.Fragment>
                                        {
                                            (parseInt(orderData.order.active) === 0) ?
                                                <Alert color="warning">To zlecenie oczekuje na aktywację przez administratora i jest widoczne tylko dla Ciebie</Alert> : null
                                        }
                                        <a href="#" onClick={this.goBack}>&lt; powrót</a>
                                        {/*<Link to="/moje-konto/zamowienia">&lt; powrót do Twoich zamówień</Link>*/}
                                    </React.Fragment>
                            }
                            <OfferInfo data={orderData.order} />
                            {
                                (orderData.order.video.length > 0) ?
                                    <Video url={`${settings.backendDomain}/orders/getvideo/${orderData.order.video}`} /> : ''
                            }
                            <GallerySection photos={orderData.photos} />
                            <OrderExtraFiles order_id={orderData.order.id} files={orderData.extraFiles} />
                        </Col>
                        <Col xs="4" className="offer-order-wrap">
                            <div className="top-info">
                                <div className="align-left">
                                    <p className="faded">ZOSTAŁO JESZCZE:</p>
                                    <TimeLeft to={GWCommon.calcOfferDeadline(orderData.order.start_date)} />
                                </div>
                                <div className="align-right">
                                    <p className="faded">TERMIN SKŁADANIA OFERT:</p>
                                    <p className="remaining">{GWCommon.calcOfferDeadline(orderData.order.start_date)}</p>
                                </div>
                            </div>
                            {
                                (this.props.userData.role === 'executor') ?
                                    <OfferWindow
                                        orderId={orderData.order.id}
                                        deadline={GWCommon.calcOfferDeadline(orderData.order.start_date)}
                                        hectareVal={orderData.order.surface}
                                        closedOrder={orderData.order.closed}
                                        updateFn={this.updateMyOffer}
                                        myOffer={this.state.myOffer}
                                        start_date={orderData.order.start_date}
                                        end_date={orderData.order.end_date}
                                    />
                                    : <OrderCustomerDetails
                                        deadline={GWCommon.calcOfferDeadline(orderData.order.start_date)}
                                        orderId={orderData.order.id}
                                        offers={this.state.offers}
                                        closedOrder={orderData.order.closed}
                                        selectWinner={this.selectWinningOffer} />
                            }
                            <LeafletMap withoutPopup center={{lat: parseFloat(orderData.order.lat),
                                lng: parseFloat(orderData.order.lng)}} data={[{
                                id: orderData.order.id,
                                lat: orderData.order.lat,
                                lng: orderData.order.lng,
                            }]} addClass="small"/>
                            {
                                (Array.isArray(this.state.nearby)) ?
                                    <NearOffers orderId={orderData.order.id} data={this.state.nearby} /> : null
                            }
                            {/*<Button className="small" onClick={this.gmapOpen}><FontAwesome name="search-plus" /> Otwórz mapę w większym oknie</Button>*/}
                            {/*<Button className="small" onClick={this.gmapPrint}><FontAwesome name="print" /> Drukuj mapę</Button>*/}
                        </Col>
                    </Row>
                </Container>
            </main>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <SingleOrder {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)