import React from 'react';
import settings from './../core/settings'

export default () => (
    <main>
        <div className="table text-center">
            <h1>BŁĄD 404</h1>
            <img src={`${settings.frontendDomain}/404.jpg`} alt="404 Strona nie znaleziona"/>
            <p>Strona której szukasz nie istnieje, albo została przeniesiona</p>
        </div>
    </main>
);
