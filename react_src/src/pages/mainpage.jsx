import React from 'react';
import { Redirect } from 'react-router-dom';
import SearchSection from '../modules/search-section/search-section';
// import SimpleMap from '../modules/google-map/map';
import LeafletMap from '../modules/leaflet-map/map';
import Listing from '../modules/list-group/listing';
import {Container, Row, Col, CustomInput} from 'reactstrap';
import { MyContext } from "../context";
import axios from 'axios';
import settings from '../core/settings';
import PopupError from "../modules/popup/popup_error";
import locutusEmpty from "locutus/php/var/empty";
import qs from 'qs';

const listOrderOptions = {
    'owp.start_date ASC': 'Termin składania ofert: od najkrótszego',
    'owp.start_date DESC': 'Termin składania ofert: od najdłuższego',
    'owp.created_at ASC': 'Data dodania: od najnowszej',
    'owp.created_at DESC': 'Data dodania: od najstarszej',
};

class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listingData: [],
            search: '',
            order: 'owp.start_date ASC',
            renderError: false,
        };
        this._ismounted = false;
        this.fetchListingPositions = this.fetchListingPositions.bind(this);
        this.setSearch = this.setSearch.bind(this);
        this.setOrder = this.setOrder.bind(this);
    }

    componentDidMount() {
        this._ismounted = true;
        if (this.props.userToken.length > 0) {
            if (locutusEmpty(this.props.userData.role) === false && this.props.userData.role !== 'customer') {
                this.fetchListingPositions();
            }
        }
    }

    setOrder(e) {
        this.setState({
            order: e.target.value,
        }, () => {
            this.fetchListingPositions();
        })
    }

    setSearch(query) {
        this.setState({
            search: query,
        }, () => {
            this.fetchListingPositions();
        })
    }

    fetchListingPositions() {
        const tmpObj = {
            orderBy: this.state.order,
        };

        const queryParams = '?' + qs.stringify(tmpObj) + ((this.state.search.length > 0) ? `&${this.state.search}` : '');
        const getQuestionsUrl = `${settings.backendDomain}/orders/listing${queryParams}`;
        const instance = axios.create({
            headers: {'Authorization': `Bearer ${this.props.userToken}`},
        });
        this.serverRequest = instance.get(getQuestionsUrl, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
            .then(response => {
                if (this._ismounted) {
                    this.setState({
                        listingData: response.data.listing,
                    });
                }
            }).catch(() => {
                if (this._ismounted) {
                    this.setState({
                        renderError: true,
                    });
                }
            });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userToken.length > 0) {
            if (locutusEmpty(this.props.userData.role) !== false && (locutusEmpty(nextProps.userData.role) === false && nextProps.userData.role !== 'customer')) {
                this.fetchListingPositions();
            }
        }
    }

    abortRequest() {
        if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
            this.serverRequest.abort();
        }
    }

    componentWillUnmount() {
        this._ismounted = false;
        this.abortRequest();
    }

    render() {
        if (this.props.userToken.length > 0) {
            if (this.props.userData.role === 'customer') {
                return <Redirect to="/moje-konto/zamowienia"/>
            }
        }
        if (this.props.userToken.length === 0) {
            return <Redirect to="/logowanie"/>
        }

        if (this.state.renderError === true) {
            return (
                <PopupError isOpen={true} title="Wystąpił błąd"><p>Wystąpił błąd aplikacji, należy odświeżyć stronę. Jeśli problem nie ustąpi prosimy o kontakt z administracją</p></PopupError>
            );
        }

        return (
            <main className="mainPage">
                <Container>
                    <Row>
                        <Col xs="9" className="map-wrap">
                            <LeafletMap data={this.state.listingData} />
                            <div className='orderSelectCnt'>
                                <CustomInput value={this.state.order} type="select" id="mainListingOrder" onChange={this.setOrder}>
                                    {
                                        Object.keys(listOrderOptions).map((val) => (
                                            <option key={val} value={val}>{listOrderOptions[val]}</option>
                                        ))
                                    }
                                </CustomInput>
                            </div>
                            <Listing data={this.state.listingData} />
                        </Col>
                        <Col xs="3" className="search-wrap">
                            <SearchSection currentSearch={this.state.search} updateData={this.setSearch} />
                        </Col>
                    </Row>
                </Container>
            </main>
        )
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <MainPage {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)
