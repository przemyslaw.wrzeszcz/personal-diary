import React from 'react';
import {Container, Row, Col, Alert} from 'reactstrap';
import {Route, Switch, Redirect, Link} from 'react-router-dom';
import locutusEmpty from "locutus/php/var/empty";
import UserMenu from '../modules/user-menu';
import UserData from '../modules/user-data';
import UserInvoice from '../modules/user-invoice';
import UserOrders from '../modules/user-orders';
import UserOrdersCompleted from '../modules/user-orders-completed';
import UserFollowed from '../modules/user-followed';
import UserAgreement from '../modules/user-agreement'
import UserAddOrder from '../modules/user-add-order';
import UserAdditionalData from '../modules/user-additional-data';
import UserOffers from '../modules/user-offers';
import { MyContext } from "../context";
import {MyUserContext} from "../user-context";

const NotCompletedAlert = ({role, location}) => {
    if (role !== 'customer') {
        return null;
    }

    return (
        <MyUserContext.Consumer>
            {({state}) => {
                return (<div className="hurrDurr">
                    {
                        ((location.pathname !== '/moje-konto/dodaj-zamowienie') && (locutusEmpty(state.uncompletedAddForm) === false)) ?
                            (<Alert color="warning">Nie ukończyłeś dodawania zlecenia. Kliknij <Link to="/moje-konto/dodaj-zamowienie">tutaj</Link> aby powrócić do formularza</Alert>) : null
                    }
                </div>);
            }}
        </MyUserContext.Consumer>
    );
};

const UserAccount = ({userToken, userData}) => {
    if (userToken.length === 0) {
        return <Redirect to="/logowanie"/>
    }
    return(
        <main className="userAccountWrap">
            <Container>
                <Row>
                    <Col xs="3">
                        <UserMenu/>
                    </Col>
                    <Col xs="9">
                        <Route render={({location}) => (
                            <React.Fragment>
                                <NotCompletedAlert location={location} role={userData.role}/>
                                <Switch key={location.key} location={location}>
                                    <Route path="/moje-konto" exact={true} component={UserData}/>
                                    <Route path="/moje-konto/zamowienia" exact={true} component={UserOrders}/>
                                    {/*<Route path="/moje-konto/zamowienia-zakonczone" exact={true} component={UserOrdersCompleted}/>*/}
                                    <Route path="/moje-konto/obserwowane" exact={true} component={UserFollowed}/>
                                    <Route path="/moje-konto/moje-oferty" exact={true} component={props => (
                                        <UserOffers {...props} won={false} />
                                    )}/>
                                    <Route path="/moje-konto/moje-oferty/wygrane" exact={true} component={props => (
                                        <UserOffers {...props} won={true} />
                                    )}/>
                                    <Route path="/moje-konto/dane-do-faktury" exact={true} component={UserInvoice}/>
                                    <Route path="/moje-konto/dane-do-umowy" exact={true} component={UserAgreement}/>
                                    <Route path="/moje-konto/dane-wykonawcy" exact={true} component={UserAdditionalData}/>
                                    <Route path="/moje-konto/dodaj-zamowienie" exact={true} component={UserAddOrder}/>
                                </Switch>
                            </React.Fragment>
                        )}/>
                    </Col>
                </Row>

            </Container>
        </main>
    );
};

export default props => (
    <MyContext.Consumer>
        {({setToken, setUser, state}) => {
            return <UserAccount {...props} userData={state.user} userToken={state.token} />
        }}
    </MyContext.Consumer>
)