import React from 'react';
import {Container, Row, Col } from 'reactstrap';
import RegisterForm from '../modules/registerForm/register-form.jsx';
import BackgroundImage from '../modules/registerForm/img/register-bg.jpg';
import axios from 'axios';

const RegisterPage = () => (
    <main className="register-page">
        <Container fluid>
            <img className="background-image" src={`/${BackgroundImage}`} alt=""/>
            <Row>
                <Col>
                    <RegisterForm />
                </Col>
            </Row>

        </Container>
    </main>
);

export default RegisterPage;