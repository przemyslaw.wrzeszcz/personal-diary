const TOTAL_COUNT = 3;

export const polskaCoords = { lat: 51.759445, lng: 19.457216 };

const sampleData = [
    {
        photo: 'https://placehold.it/120x80',
        subject: 'MULCZOWANIE',
        where: 'JELCZ LASKOWICE, DOLNOŚLĄSKIE',
        surface: '14',
        distance: '34km',
        when: '2018-08-20',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'REKULTYWACJA',
        where: 'SZCZECIN, ZACHODNIOPOMORSKIE',
        surface: '5',
        distance: '32km',
        when: '2018-07-20T20:10:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'REKULTYWACJA',
        where: 'DARłOWO, ZACHODNIOPOMORSKIE',
        surface: '3',
        distance: '30km',
        when: '2018-09-21T18:10:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'STABILIZACJA GRUNTU',
        where: 'GDAŃSK, POMORSKIE',
        surface: '8',
        distance: '34km',
        when: '2018-10-22T20:20:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'REKULTYWACJA',
        where: 'KATOWICE, ŚLĄSKIE',
        surface: '8',
        distance: '34km',
        when: '2018-07-20T20:10:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'WYCINKA',
        where: 'BORÓW, DOLNOŚLĄSKIE',
        surface: '2',
        distance: '34km',
        when: '2018-07-15T20:10:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'MULCZOWANIE',
        where: 'SOBÓTKA, DOLNOŚLĄSKIE',
        surface: '4',
        distance: '34km',
        when: '2018-09-21T20:10:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'STABILIZACJA GRUNTU',
        where: 'WARSZAWA, MAZOWIECKIE',
        surface: '12',
        distance: '34km',
        when: '2018-08-15T20:10:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'MULCZOWANIE',
        where: 'OPOLE, OPOLSKIE',
        surface: '2',
        distance: '34km',
        when: '2018-09-20T20:10:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'REKULTYWACJA',
        where: 'MIĘDZYZDROJE, POMORSKIE',
        surface: '8',
        distance: '34km',
        when: '2018-11-20T20:10:00',
    },
    {
        photo: 'https://placehold.it/120x80',
        subject: 'WYCINKA',
        distance: '34km',
        where: 'BIAŁYSTOK, PODLASKIE',
        surface: '40',
        when: '2018-12-24T23:10:00',
    },
];

export const listingData = [...Array(TOTAL_COUNT)]
  .fill(0) // fill(0) for loose mode
  .map((__, index) => {
    let dataNo = (index % 2);

    if(typeof sampleData[index] !== 'undefined') {
        dataNo = index;
    }

    return Object.assign({
        id: index,
        lat:
        polskaCoords.lat +
        0.01 *
        index *
        Math.sin(30 * Math.PI * index / 180) *
        Math.cos(50 * Math.PI * index / 180) +
        Math.sin(5 * index / 180),
        lng:
        polskaCoords.lng +
        0.01 *
        index *
        Math.cos(70 + 23 * Math.PI * index / 180) *
        Math.cos(50 * Math.PI * index / 180) +
        Math.sin(5 * index / 180),
    }, sampleData[dataNo]);
  });
