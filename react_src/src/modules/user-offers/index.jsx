import React from 'react';
import { MyContext } from "../../context";
import Listing from "../list-group/listing";
import axios from 'axios';
import settings from '../../core/settings';
import PopupError from "../../modules/popup/popup_error";
import locutusEmpty from "locutus/php/var/empty";

class UserOffers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listingData: [],
            renderError: false,
        };
        this.getListing = this.getListing.bind(this);
        this.abortRequest = this.abortRequest.bind(this);
    }

    componentDidMount() {
        if (this.props.userToken.length > 0) {
            if (locutusEmpty(this.props.userData.role) === false && this.props.userData.role !== 'customer' && !this.props.button) {
                this.getListing(this.props.won);
            }
        }
    }

    abortRequest() {
        if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
            this.serverRequest.abort();
        }
    }

    getListing(won = false) {
        const wonUrl = (won === true) ? '/selected' : '';
        const getOrdersWatchedUrl = `${settings.backendDomain}/orders/offers${wonUrl}`;
        const instance = axios.create({
            headers: {'Authorization': `Bearer ${this.props.userToken}`},
        });
        this.serverRequest = instance.get(getOrdersWatchedUrl, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
            .then(response => {
                this.setState({
                    renderError: false,
                    listingData: response.data.listing,
                });
            }).catch((error) => {
                this.setState({
                    renderError: true,
                });
            });
    }

    componentWillUnmount() {
        this.abortRequest();
    }

    componentWillReceiveProps(nextProps) {
        this.abortRequest();

        if (nextProps.userToken.length > 0) {
            if (nextProps.userData.role !== 'customer') {
                this.getListing(nextProps.won);
            }
        }
    }

    render() {
        if (this.state.renderError) {
            return (
                <PopupError isOpen={true} title="Wystąpił błąd"><p>Wystąpił błąd aplikacji, należy odświeżyć stronę. Jeśli problem nie ustąpi prosimy o kontakt z administracją</p></PopupError>
            );
        }

        return (
            <React.Fragment>
                <Listing data={this.state.listingData} />
            </React.Fragment>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <UserOffers {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)
