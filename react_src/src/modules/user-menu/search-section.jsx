import React, {Component} from 'react';
import {
    Collapse,
    CardBody,
    Card,
    FormGroup,
    Label,
    Input,
    Form,
    CustomInput,
    Button,
    InputGroup,
    InputGroupAddon,
} from 'reactstrap';
import FontAwesome from 'react-fontawesome';

class SearchSection extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {collapse: false};
    }

    toggle() {
        this.setState({collapse: !this.state.collapse});
    }

    render() {
        return (
            <aside className="search-section section-background">
                <InputGroup>
                    <Input placeholder="wyszukaj"/>
                    <InputGroupAddon addonType="append">
                        <Button color="primary"><FontAwesome name="search"/></Button>
                    </InputGroupAddon>
                </InputGroup>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>BRANŻA:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Mulczowanie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Rekultywacja
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Stabilizacja Gruntu
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Wycinka
                                    </Label>
                                </FormGroup>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>WOJEWÓDZTWO:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Dolnośląskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Kujawsko-Pomorskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Lubelskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Lubuskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Łódzkie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Małopolskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Mazowieckie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Opolskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Podkarpackie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Podlaskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Pomorskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Śląskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Świętokrzyskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Warmińsko-Mazurskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Wielkopolskie
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Zachodniopomorskie
                                    </Label>
                                </FormGroup>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>POWIERZCHNIA:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody className="SmallerInputs">
                                <Form inline>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="lowNumber" className="mr-sm-2 faded">Od</Label>
                                        <Input type="number" name="lowerNumber" id="lowNumber"/>
                                        <Label for="lowNumber" className="mr-sm-2">H</Label>
                                    </FormGroup>
                                    <span className="separator">-</span>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="highNumber" className="mr-sm-2 faded">Do</Label>
                                        <Input type="number" name="higherNumber" id="highNumber"/>
                                        <Label for="highNumber" className="mr-sm-2">H</Label>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>SZTUK DRZEW:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody className="SmallerInputs">
                                <Form inline>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="lowTree" className="mr-sm-2 faded">Od</Label>
                                        <Input type="number" name="lowerTree" id="lowTree"/>
                                        <Label for="lowTree" className="mr-sm-2">szt.</Label>
                                    </FormGroup>
                                    <span className="separator">-</span>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="highTree" className="mr-sm-2 faded">Do</Label>
                                        <Input type="number" name="higherTree" id="highTree"/>
                                        <Label for="highTree" className="mr-sm-2">szt.</Label>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>POZOSTAŁY CZAS SKŁADANIA OFERT:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody className="SmallerInputs">
                                <Form>
                                    <FormGroup>
                                        <CustomInput type="select" id="exampleCustomSelect" name="customSelect">
                                            <option selected disabled>---wybierz---</option>
                                            <option>Value 1</option>
                                            <option>Value 2</option>
                                            <option>Value 3</option>
                                            <option>Value 4</option>
                                            <option>Value 5</option>
                                        </CustomInput>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>TERMIN ZAKOŃCZENIA PRAC:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody className="SmallerInputs">
                                <Form inline>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="lowDate" className="mr-sm-2 faded">Od</Label>
                                        <Input type="date" name="lowerDate" id="lowDate"/>
                                    </FormGroup>
                                    <span className="separator">-</span>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="highDate" className="mr-sm-2 faded">Do</Label>
                                        <Input type="date" name="higherDate" id="highDate"/>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>STOPIEŃ TRUDNOŚCI:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Bardzo łatwy
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Łatwy
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Średni
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Trudny
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Bardzo trudny
                                    </Label>
                                </FormGroup>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>TERMIN ROZPOCZĘCIA PRAC:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody className="SmallerInputs">
                                <Form inline>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="lowStartDate" className="mr-sm-2 faded">Od</Label>
                                        <Input type="date" name="lowerStartDate" id="lowStartDate"/>
                                    </FormGroup>
                                    <span className="separator">-</span>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Label for="highStartDate" className="mr-sm-2 faded">Do</Label>
                                        <Input type="date" name="higherStartDate" id="highStartDate"/>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>RODZAJ TERENU:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Górzysty
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Nizinny
                                    </Label>
                                </FormGroup>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <div className="element">
                    <a href="#" className="SearchToggle" onClick={this.toggle}>GATUNKI DRZEW:</a>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Iglaste
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Liściaste
                                    </Label>
                                </FormGroup>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>
                <Button color="primary" size="lg" block>FILTRUJ</Button>
            </aside>
        );
    }
}

export default SearchSection;
