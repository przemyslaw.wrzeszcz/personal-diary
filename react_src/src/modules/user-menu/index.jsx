import React, {Component} from 'react';
import { NavLink, Link} from 'react-router-dom';
import {Nav,NavItem,Button} from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import { MyContext } from "../../context";

class Index extends Component {
    render() {
        return (
            <aside className="section-background user-menu">
                <div>
                    <h5>Moje konto</h5>
                    <Nav vertical pills>
                        <NavItem>
                            <NavLink exact={true} to="/moje-konto" activeClassName="active" className="nav-link">Moje dane</NavLink>
                        </NavItem>
                        {(this.props.userData.role === 'executor') ?
                            (
                                <div className="wrapper">
                                    <NavItem>
                                        <NavLink exact={true} to="/moje-konto/obserwowane" activeClassName="active" className="nav-link">Obserwowane</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink exact={true} to="/moje-konto/moje-oferty" activeClassName="active" className="nav-link">Złożone oferty</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink exact={true} to="/moje-konto/moje-oferty/wygrane" activeClassName="active" className="nav-link">Zwycięskie oferty</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink exact={true} to="/moje-konto/dane-do-faktury" activeClassName="active" className="nav-link">Dane do faktury</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink exact={true} to="/moje-konto/dane-wykonawcy" activeClassName="active" className="nav-link">Dane wykonawcy</NavLink>
                                    </NavItem>
                                </div>
                            ): null
                        }
                        {(this.props.userData.role === 'customer') ?
                            <div className="wrapper">
                                <NavItem>
                                    <NavLink exact={true} to="/moje-konto/zamowienia" activeClassName="active" className="nav-link">Moje zamówienia</NavLink>
                                </NavItem>
                                {/*<NavItem>
                                    <NavLink exact={true} to="/moje-konto/zamowienia-zakonczone" activeClassName="active" className="nav-link">Zakończone zamówienia</NavLink>
                                </NavItem>*/}
                                <NavItem>
                                    <NavLink exact={true} to="/moje-konto/dane-do-umowy" activeClassName="active" className="nav-link">Dane do umowy</NavLink>
                                </NavItem>
                                <Link className="btn btn-tertiary btn-block" exact="true" to="/moje-konto/dodaj-zamowienie">Dodaj zamówienie</Link>
                            </div>: null
                        }
                    </Nav>
                </div>
            </aside>
        );
    }
}


export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <Index {...props} userData={state.user} />
        }}
    </MyContext.Consumer>
)