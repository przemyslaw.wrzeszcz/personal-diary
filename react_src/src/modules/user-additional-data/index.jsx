import React, {Component} from 'react';
import {Form, FormGroup, Row, Tooltip, Col, Label, Input, Button, Alert} from 'reactstrap';
import axios from 'axios';
import { MyContext } from "../../context";
import {validateInput}  from '../validation/userAddData';
import scrollToComponent from 'react-scroll-to-component';
import DynamicInputs from '../user-add-inputs/index';
import settings from '../../core/settings';

class Index extends Component {
    constructor(props) {
        super(props);
        let toAssign = {};
        const userExtra = props.userData.extra_data;

        if (userExtra !== null && typeof userExtra === 'object') {
            if (typeof userExtra.area_province !== 'undefined') {
                toAssign = Object.assign(toAssign, props.userData.extra_data.area_province);
            }

            if (typeof userExtra.executor_skills !== 'undefined') {
                toAssign = Object.assign(toAssign, props.userData.extra_data.executor_skills);
            }

            toAssign.years_experience = (userExtra.years_experience) ? userExtra.years_experience : '';
            toAssign.completed_orders = (userExtra.completed_orders) ? userExtra.completed_orders : '';
            toAssign.finished_order_size = (userExtra.finished_order_size) ? userExtra.finished_order_size : '';
            toAssign.area_type = (userExtra.area_type) ? userExtra.area_type : null;
            toAssign.type_zone_value = (userExtra.type_zone_value) ? userExtra.type_zone_value : '';
            toAssign.machine_park = (userExtra.machine_park) ? userExtra.machine_park : null;
            toAssign.tags = (userExtra.machines) ? userExtra.machines : [];
        }

        this.state = Object.assign({
            base_address: props.userData.base_address,
            years_experience: null,
            completed_orders: null,
            finished_order_size: null,
            area_type: null,
            type_zone_value: null,
            dolnoslaskie: false,
            kujawsko_pomorskie: false,
            lubelskie: false,
            lubuskie: false,
            lodzkie: false,
            malopolskie: false,
            mazowieckie: false,
            opolskie: false,
            podkarpackie: false,
            podlaskie: false,
            pomorskie: false,
            slaskie: false,
            swietokrzyskie: false,
            warminsko_mazurskie: false,
            wielkopolskie: false,
            zachodniopomorskie: false,
            cutting_trees: false,
            removing_bushes: false,
            removing_selfsows: false,
            removing_branches: false,
            removing_orchard: false,
            removing_plantation: false,
            removing_sticks: false,
            mineralization: false,
            creating_belts: false,
            other_work: '',
            max_trunk_diameter_mulcz: '',
            max_trunk_diameter_frez: '',
            max_frez_depth: '',
            number_trees_cut: '',
            number_bushes_removed: '',
            number_branches_removed: '',
            number_small_bushes_removed: '',
            number_mineralization: '',
            number_creation: '',
            machine_park: null,
            tags: [],
            isLoading: false,
            serwerErrorMsg: '',
            serwerSuccessMsg: '',
            errors: {},
            refPassed: false,
        }, toAssign);
        this.myRef = React.createRef();
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.setTags = this.setTags.bind(this);
        this.controlledInputValue = this.controlledInputValue.bind(this);
    }

    setTags(tags) {
        this.setState({tags});
    }

    onChange(e) {
        const target = e.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;

        const stateToSet = (name === 'area_type' && value !== 'province') ?
            {
                [name]: value,
                dolnoslaskie: false,
                kujawsko_pomorskie: false,
                lubelskie: false,
                lubuskie: false,
                lodzkie: false,
                malopolskie: false,
                mazowieckie: false,
                opolskie: false,
                podkarpackie: false,
                podlaskie: false,
                pomorskie: false,
                slaskie: false,
                swietokrzyskie: false,
                warminsko_mazurskie: false,
                wielkopolskie: false,
                zachodniopomorskie: false,
            } :
            {
                [name]: value,
            };

        this.setState(stateToSet);
    }
    isValid() {
        const {errors, isValid} = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }
        return isValid;
    }

    controlledInputValue(name) {
        return (this.state[name]) ? this.state[name] : ''
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            const extraApiUrl = `${settings.backendDomain}/users/extra`;
            const payload = {
                "base_address": this.state.base_address,
                "extra_data": {
                    "years_experience": this.state.years_experience,
                    "completed_orders": this.state.completed_orders,
                    "finished_order_size": this.state.finished_order_size,
                    "area_type": this.state.area_type,
                    "area_province": {
                        "dolnoslaskie": this.state.dolnoslaskie,
                        "kujawsko_pomorskie": this.state.kujawsko_pomorskie,
                        "lubelskie": this.state.lubelskie,
                        "lubuskie": this.state.lubuskie,
                        "lodzkie": this.state.lodzkie,
                        "malopolskie": this.state.malopolskie,
                        "mazowieckie": this.state.mazowieckie,
                        "opolskie": this.state.opolskie,
                        "podkarpackie": this.state.podkarpackie,
                        "podlaskie": this.state.podlaskie,
                        "pomorskie": this.state.pomorskie,
                        "slaskie": this.state.slaskie,
                        "swietokrzyskie": this.state.swietokrzyskie,
                        "warminsko_mazurskie": this.state.warminsko_mazurskie,
                        "wielkopolskie": this.state.wielkopolskie,
                        "zachodniopomorskie": this.state.zachodniopomorskie,
                    },
                    "type_zone_value": this.state.type_zone_value,
                    "executor_skills": {
                        "cutting_trees": this.state.cutting_trees,
                        "removing_bushes": this.state.removing_bushes,
                        "removing_selfsows": this.state.removing_selfsows,
                        "removing_branches": this.state.removing_branches,
                        "removing_orchard": this.state.removing_orchard,
                        "removing_plantation": this.state.removing_plantation,
                        "removing_sticks": this.state.removing_sticks,
                        "mineralization": this.state.mineralization,
                        "creating_belts": this.state.creating_belts,
                        "other_work": this.state.other_work,
                        "max_trunk_diameter_mulcz": this.state.max_trunk_diameter_mulcz,
                        "max_trunk_diameter_frez": this.state.max_trunk_diameter_frez,
                        "max_frez_depth": this.state.max_frez_depth,
                    },
                    "number_trees_cut": this.state.number_trees_cut,
                    "number_bushes_removed": this.state.number_bushes_removed,
                    "number_branches_removed": this.state.number_branches_removed,
                    "number_small_bushes_removed": this.state.number_small_bushes_removed,
                    "number_mineralization": this.state.number_mineralization,
                    "number_creation": this.state.number_creation,
                    "machines": this.state.tags,
                    "machine_park": this.state.machine_park,
                },
            };
            const instance = axios.create({
                headers: {'Authorization': `Bearer ${this.props.userToken}`},
            });
            instance.put(extraApiUrl, payload, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                .then((response) => {
                    if (response.data.success) {
                        this.setState({
                            serwerSuccessMsg: 'Zmiany wprowadzone pomyślnie!',
                            errors: {},
                            serwerErrorMsg: '',
                        });
                        const newUser = Object.assign(this.props.userData, payload);
                        this.props.setUser(newUser);

                        scrollToComponent(this.myRef.current, {
                            align: 'top',
                            duration: 1,
                        });
                        return;
                    }

                    const errorMaping = {
                        "base_address": 'base_address',
                    };
                    const errors = {};
                    Object.keys(response.data.errors).forEach((key) => {
                        errors[errorMaping[key]] = response.data.errors[key];
                    });
                    this.setState({ errors });
                })
                .catch((error) => {
                    this.setState({serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.'});

                    scrollToComponent(this.myRef.current, {
                        align: 'top',
                        duration: 1,
                    });
                });
        }
    }
    render() {
        const {
            isLoading,
            serwerErrorMsg,
            serwerSuccessMsg,
            errors,
            area_type,
            dolnoslaskie,
            kujawsko_pomorskie,
            lubelskie,
            lubuskie,
            lodzkie,
            malopolskie,
            mazowieckie,
            opolskie,
            podkarpackie,
            podlaskie,
            pomorskie,
            slaskie,
            swietokrzyskie,
            warminsko_mazurskie,
            wielkopolskie,
            zachodniopomorskie,
            cutting_trees,
            removing_bushes,
            removing_selfsows,
            removing_branches,
            removing_orchard,
            removing_plantation,
            removing_sticks,
            mineralization,
            creating_belts,
            machine_park,
            tags,
        } = this.state;
        return (
            <section className="userData" ref={this.myRef}>
                <Form onSubmit={this.onSubmit}>
                    <Row>
                        <Col xs="12">
                            <h5>Dane wykonawcy</h5>

                            {
                                (serwerErrorMsg) ? (
                                    <Alert color="danger">{serwerErrorMsg}</Alert>
                                ) : null
                            }
                            {
                                (serwerSuccessMsg) ? (
                                    <Alert color="success">{serwerSuccessMsg}</Alert>
                                ) : null
                            }
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <FormGroup row>
                                <Label for="base_address" sm="12">Adres siedziby</Label>
                                <Col sm="12">
                                    <Input type="text" value={this.controlledInputValue('base_address')} name="base_address" id="base_address" onChange={this.onChange} />
                                    <span className="error">{errors.base_address}</span>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="userYearsExp" sm="12">Doświadczenie w branży w latach</Label>
                                <Col sm="12">
                                    <Input type="number" value={this.controlledInputValue('years_experience')} name="years_experience" id="userYearsExp" onChange={this.onChange} />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label for="userFinishedOrders" sm="12">Przybliżona liczba wykonanych zleceń</Label>
                                <Col sm="12">
                                    <Input type="number" value={this.controlledInputValue('completed_orders')} name="completed_orders" id="userFinishedOrders" onChange={this.onChange} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="userFinishedOrdersSize" sm="12">Przybliżona łączna powierzchnia wykonanych zleceń - w hektarach</Label>
                                <Col sm="12">
                                    <Input type="number" value={this.controlledInputValue('finished_order_size')} name="finished_order_size" id="userFinishedOrdersSize" onChange={this.onChange} />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label sm="12" className="highlight">Obszar działania</Label>
                                <Col xs="12" md="12">
                                    <Row>
                                        <FormGroup check>
                                            <Label check sm="12">
                                                <Input type="radio" value='whole_country' checked={(area_type === 'whole_country')} name="area_type" onChange={this.onChange} />{' '}
                                                Cała Polska
                                            </Label>
                                        </FormGroup>
                                    </Row>
                                    <Row>
                                        <FormGroup check>
                                            <Label check sm="12">
                                                <Input type="radio" value='province' checked={(area_type === 'province')} name="area_type" onChange={this.onChange} />{' '}
                                                Województwo/a
                                            </Label>
                                        </FormGroup>
                                    </Row>
                                    <Row>
                                        <Col xs="12" md="12" style={{
                                            display: (area_type !== 'province') ? 'none' : '',
                                        }}>
                                            <Col xs="12" md="12">
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(dolnoslaskie)} name="dolnoslaskie"  onChange={this.onChange} />{' '}
                                                        Dolnośląskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(kujawsko_pomorskie)} name="kujawsko_pomorskie" onChange={this.onChange} />{' '}
                                                        Kujawsko-Pomorskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(lubelskie)} name="lubelskie" onChange={this.onChange} />{' '}
                                                        Lubelskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(lubuskie)} name="lubuskie" onChange={this.onChange} />{' '}
                                                        Lubuskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(lodzkie)} name="lodzkie" onChange={this.onChange} />{' '}
                                                        Łódzkie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(malopolskie)} name="malopolskie" onChange={this.onChange} />{' '}
                                                        Małopolskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(mazowieckie)} name="mazowieckie" onChange={this.onChange} />{' '}
                                                        Mazowieckie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(opolskie)} name="opolskie" onChange={this.onChange} />{' '}
                                                        Opolskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(podkarpackie)} name="podkarpackie" onChange={this.onChange} />{' '}
                                                        Podkarpackie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(podlaskie)} name="podlaskie" onChange={this.onChange} />{' '}
                                                        Podlaskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(pomorskie)} name="pomorskie" onChange={this.onChange} />{' '}
                                                        Pomorskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(slaskie)} name="slaskie" onChange={this.onChange} />{' '}
                                                        Śląskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(swietokrzyskie)} name="swietokrzyskie" onChange={this.onChange} />{' '}
                                                        Świętokrzyskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(warminsko_mazurskie)} name="warminsko_mazurskie" onChange={this.onChange} />{' '}
                                                        Warmińsko-Mazurskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(wielkopolskie)} name="wielkopolskie" onChange={this.onChange} />{' '}
                                                        Wielkopolskie
                                                    </Label>
                                                </FormGroup>
                                                <FormGroup check>
                                                    <Label check>
                                                        <Input type="checkbox" value='1' checked={!!(zachodniopomorskie)} name="zachodniopomorskie" onChange={this.onChange} />{' '}
                                                        Zachodniopomorskie
                                                    </Label>
                                                </FormGroup>
                                            </Col>
                                        </Col>
                                        <FormGroup check>
                                            <Label check sm="12">
                                                <Input type="radio" value='area_type_zone' checked={(area_type === 'area_type_zone')} name="area_type" onChange={this.onChange} />{' '}
                                                W promieniu: <Input className="inline" value={this.controlledInputValue('type_zone_value')} type="number"
                                                    name="type_zone_value" id="type_zone_value" onChange={this.onChange} /> kilometrów od siedziby
                                            </Label>
                                        </FormGroup>
                                    </Row>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label className="highlight" sm="12">Prace które mogę wykonywać</Label>
                                <Col sm="12">
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="cutting_trees" value='1' checked={!!(cutting_trees)} onChange={this.onChange} />{' '}
                                            Wycinka drzew/ek
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="removing_bushes" value='1' checked={!!(removing_bushes)} onChange={this.onChange} />{' '}
                                            Powierzchniowe usuwanie krzaków (mulczowanie)
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="removing_selfsows" value='1' checked={!!(removing_selfsows)} onChange={this.onChange} />{' '}
                                            Powierzchniowe usuwanie samosiewów (mulczowanie)
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="removing_branches" value='1' checked={!!(removing_branches)} onChange={this.onChange} />{' '}
                                            Usuwanie korzeni (rekultywacja lub punktowe frezowanie pni)
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="removing_orchard" value='1' checked={!!(removing_orchard)} onChange={this.onChange} />{' '}
                                            Likwidacja sadu (mulczowanie + rekultywacja)
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="removing_plantation" value='1' checked={!!(removing_plantation)} onChange={this.onChange} />{' '}
                                            Likwidacja plantacji. (mulczowanie + rekultywacja)
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="removing_sticks" value='1' checked={!!(removing_sticks)} onChange={this.onChange} />{' '}
                                            Likwidacja stosów gałęzi (mulczowanie)
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="mineralization" value='1' checked={!!(mineralization)} onChange={this.onChange} />{' '}
                                            Mineralizacja pasów ppoż.
                                        </Label>
                                    </FormGroup>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" name="creating_belts" value='1' checked={!!(creating_belts)} onChange={this.onChange} />{' '}
                                            Tworzenie pasów ppoż. (rekultywacja / frezowanie pni)
                                        </Label>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label for="other_work" sm="12">Inne</Label>
                                        <Col sm="12">
                                            <Input type="text" value={this.controlledInputValue('other_work')} name="other_work" id="other_work" onChange={this.onChange} />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label for="max_trunk_diameter_mulcz" sm="12">Maksymalna średnica pnia do mulczowania – w cm.</Label>
                                        <Col sm="12">
                                            <Input type="number" value={this.controlledInputValue('max_trunk_diameter_mulcz')} name="max_trunk_diameter_mulcz" id="max_trunk_diameter_mulcz" onChange={this.onChange} />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label for="max_trunk_diameter_frez" sm="12">Maksymalna średnica pnia do frezowania (rekultywacji) – w cm.</Label>
                                        <Col sm="12">
                                            <Input type="number" value={this.controlledInputValue('max_trunk_diameter_frez')} name="max_trunk_diameter_frez" id="max_trunk_diameter_frez" onChange={this.onChange} />
                                        </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                        <Label for="max_frez_depth" sm="12">Maksymalna głębokość frezowania / rekultywacji.</Label>
                                        <Col sm="12">
                                            <Input type="number" value={this.controlledInputValue('max_frez_depth')} name="max_frez_depth" id="max_frez_depth" onChange={this.onChange} />
                                        </Col>
                                    </FormGroup>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label sm="12" className="highlight">Wydajność. Jakie powierzchnie/wielkości w przybliżeniu jesteś w stanie zrobić dziennie</Label>
                                <Col xs="12" md="12">
                                    <Row>
                                        <Col sm="12">
                                            <FormGroup row>
                                                <Label for="number_trees_cut" sm="12">Wycinka drzew/ek : liczba drzew</Label>
                                                <Col sm="12">
                                                    <Input type="number" value={this.controlledInputValue('number_trees_cut')} name="number_trees_cut" id="number_trees_cut" onChange={this.onChange} />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Label for="number_bushes_removed" sm="12">Powierzchniowe usuwanie krzaków, samosiewów (mulczowanie) : powierzchnia w hektarach.</Label>
                                                <Col sm="12">
                                                    <Input type="number" value={this.controlledInputValue('number_bushes_removed')} name="number_bushes_removed" id="number_bushes_removed" onChange={this.onChange} />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Label for="number_branches_removed" sm="12">
                                                    Usuwanie korzeni o średniej średnicy ok. 30 cm i regularnym występowaniu na całej powierzchni
                                                    (rekultywacja lub punktowe frezowanie pni) : powierzchnia w hektarach.
                                                </Label>
                                                <Col sm="12">
                                                    <Input type="number" value={this.controlledInputValue('number_branches_removed')} name="number_branches_removed" id="number_branches_removed" onChange={this.onChange} />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Label for="number_small_bushes_removed" sm="12">
                                                    Usuwanie powierzchniowe krzaków i samosiewów o średniej
                                                    średnicy pnia ok 10 cm i rekultywacja do 20 cm w głąb (mulczowanie + rekultywacja) :  powierzchnia w hektarach.
                                                </Label>
                                                <Col sm="12">
                                                    <Input type="number" value={this.controlledInputValue('number_small_bushes_removed')} name="number_small_bushes_removed"
                                                        id="number_small_bushes_removed" onChange={this.onChange} />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Label for="number_mineralization" sm="12">Mineralizacja pasów ppoż. : powierzchnia w hektarach.</Label>
                                                <Col sm="12">
                                                    <Input type="number" value={this.controlledInputValue('number_mineralization')} name="number_mineralization" id="number_mineralization" onChange={this.onChange} />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Label for="number_mineralization" sm="12">Tworzenie pasów ppoż. : powierzchnia w hektarach.</Label>
                                                <Col sm="12">
                                                    <Input type="number" value={this.controlledInputValue('number_creation')} name="number_creation" id="number_creation" onChange={this.onChange} />
                                                </Col>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label sm="12" className="highlight">Posiadane maszyny</Label>
                                <Col xs="12" md="12">
                                    <DynamicInputs tags={tags} setTags={this.setTags} />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label sm="12" className="highlight">Chęć rozwoju / rozbudowa parku maszynowego?</Label>
                                <Col xs="12" md="12">
                                    <Row>
                                        <FormGroup check>
                                            <Label check sm="12">
                                                <Input type="radio" value='yes' checked={(machine_park === 'yes')} name="machine_park" onChange={this.onChange} />{' '}
                                                Tak
                                            </Label>
                                        </FormGroup>
                                    </Row>
                                    <Row>
                                        <FormGroup check>
                                            <Label check sm="12">
                                                <Input type="radio" value='no' checked={(machine_park === 'no')} name="machine_park" onChange={this.onChange} />{' '}
                                                Nie
                                            </Label>
                                        </FormGroup>
                                    </Row>
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <div className="text-center">
                                <Button color="primary" type="submit">Zapisz</Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </section>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({setUser, state}) => {
            return <Index {...props} setUser={setUser} userData={state.user}  userToken={state.token} />
        }}
    </MyContext.Consumer>
)
