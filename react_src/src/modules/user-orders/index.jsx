import React, {Component} from 'react';
import { Alert } from 'reactstrap';
import Listing from '../list-group/listing';
import { MyContext } from "../../context";
import axios from 'axios';
import settings from '../../core/settings';
import {Link} from 'react-router-dom';
import locutusEmpty from "locutus/php/var/empty";

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listingData: [],
            renderError: false,
            visible: true,
            sessionMessage: '',
        };
        this.onDismiss = this.onDismiss.bind(this);
    }
    onDismiss() {
        this.setState({ visible: false });
    }

    componentDidMount() {
        if (this.props.userToken.length > 0) {
            if (this.props.userData.role !== 'executor') {
                const getQuestionsUrl = `${settings.backendDomain}/orders/client/listing`;
                const instance = axios.create({
                    headers: {'Authorization': `Bearer ${this.props.userToken}`},
                });
                instance.get(getQuestionsUrl, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                    .then(response => {
                        this.setState({
                            listingData: response.data.listing,
                        });
                    }).catch((error) => {
                        this.setState({
                            renderError: true,
                        });
                    });

                const newMsg = this.props.getSessionMessage();

                if (locutusEmpty(newMsg) === false && newMsg !== this.state.sessionMessage) {
                    this.setState({
                        sessionMessage: newMsg,
                    });
                }
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        const newMsg = nextProps.getSessionMessage();

        if ((locutusEmpty(newMsg) === false) && (newMsg !== this.state.sessionMessage)) {
            this.setState({
                sessionMessage: newMsg,
            });
        }
    }

    render() {

        return (
            <React.Fragment>
                {(this.state.listingData.length > 0) ?
                    <React.Fragment>
                        {(this.state.sessionMessage.length > 0) ?
                            <Alert color="success" isOpen={this.state.visible} toggle={this.onDismiss}>
                                {this.state.sessionMessage}
                            </Alert>
                            : null
                        }
                        <Listing data={this.state.listingData} myOrders />
                    </React.Fragment>:
                    <h5>Nie dodałeś jeszcze żadnego zlecenia.<br/> Możesz dodać je <Link to="/moje-konto/dodaj-zamowienie">tutaj</Link></h5>
                }

            </React.Fragment>
        );
    }
}


export default props => (
    <MyContext.Consumer>
        {({state, getSessionMessage}) => {
            return <Index {...props} userToken={state.token} userData={state.user} getSessionMessage={getSessionMessage} />
        }}
    </MyContext.Consumer>
)

