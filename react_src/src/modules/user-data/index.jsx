import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import {Form, FormGroup, Row, Col, Label, Input, Button, Alert} from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import {validateInput}  from '../validation/userData';
import axios from 'axios';
import { MyContext } from "../../context";
import settings from '../../core/settings';

class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.userData.name,
            surname: props.userData.surname,
            mail: props.userData.mail,
            phone: props.userData.phone,
            old_password: '',
            password: '',
            rpassword: '',
            errors: {},
            isLoading: false,
            serwerErrorMsg: '',
            serwerSuccessMsg: '',

        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    isValid() {
        const {errors, isValid} = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }
        return isValid;
    }
    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            const registerApiUrl = `${settings.backendDomain}/users/register`;
            const payload={
                "name": this.state.name,
                "surname": this.state.surname,
                "phone": this.state.phone,
                "mail": this.state.mail,
                "old_password": this.state.old_password,
                "password": this.state.password,
                "rpassword": this.state.rpassword,
            };

            const instance = axios.create({
                headers: {'Authorization': `Bearer ${this.props.userToken}`},
            });

            instance.put(registerApiUrl, payload, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                .then((response) => {
                    if (response.data.success) {
                        this.setState({
                            serwerSuccessMsg: 'Zmiany wprowadzone pomyślnie!',
                            errors: {},
                            serwerErrorMsg: '',
                            old_password: '',
                            password: '',
                            rpassword: '',
                        });
                        const newUser = this.props.userData;
                        newUser.name = payload.name;
                        newUser.surname = payload.surname;
                        newUser.phone = payload.phone;
                        newUser.mail = payload.mail;
                        this.props.setUser(newUser);
                        return;
                    }

                    const errorMaping = {
                        "name": 'name',
                        "surname": 'surname',
                        "nip": 'nip',
                        "phone": 'phone',
                        "mail": 'email',
                        "password": 'password',
                        "rpassword": 'rpassword',
                        "old_password": "old_password",
                    };
                    const errors = {};
                    Object.keys(response.data.errors).forEach((key) => {
                        errors[errorMaping[key]] = response.data.errors[key];
                    });
                    this.setState({ errors });
                })
                .catch((error) => {
                    this.setState({ serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.' });
                    // jak tutaj wpadamy to pokaż userowi komunikat że wystąpił nieoczekiwany błąd i niech spróbuje ponownie
                });
        }
    }
    render() {
        const {name, surname, mail, phone, errors, old_password, password, rpassword, isLoading, serwerErrorMsg, serwerSuccessMsg} = this.state;
        return (
            <section className="userData">
                <Form onSubmit={this.onSubmit}>
                    <Row>
                        <Col xs="12">
                            <h5>Dane główne</h5>
                            {
                                (serwerErrorMsg) ? (
                                    <Alert color="danger">{serwerErrorMsg}</Alert>
                                ) : null
                            }
                            {
                                (serwerSuccessMsg) ? (
                                    <Alert color="success">{serwerSuccessMsg}</Alert>
                                ) : null
                            }
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <FormGroup row>
                                <Label for="name" sm="3">Imię</Label>
                                <Col sm="9">
                                    <Input value={name} type="text" name="name" id="userName" onChange={this.onChange} />
                                    <span className="error">{errors.name}</span>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="surname" sm="3">Nazwisko</Label>
                                <Col sm="9">
                                    <Input value={surname} type="text" name="surname" id="userSurname" onChange={this.onChange} />
                                    <span className="error">{errors.surname}</span>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="mail" sm="3">Email</Label>
                                <Col sm="9">
                                    <Input value={mail} type="email" name="mail" id="mail" onChange={this.onChange} />
                                    <span className="error">{errors.mail}</span>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="phone" sm="3">Telefon</Label>
                                <Col sm="9">
                                    <Input type="number" value={phone} name="phone" id="phone" onChange={this.onChange} />
                                    <span className="error">{errors.phone}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label for="currentPassword" sm="3">Obecne hasło</Label>
                                <Col sm="9">
                                    <Input type="password" name="old_password" id="currentPassword" value={old_password} error={errors.old_password} onChange={this.onChange} />
                                    <span className="error">{errors.old_password}</span>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="newPassword" sm="3">Nowe hasło</Label>
                                <Col sm="9">
                                    <Input type="password" name="password" id="newPassword" value={password} error={errors.password} onChange={this.onChange} />
                                    <span className="error">{errors.password}</span>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="repeatPassword" sm="3">Powtórz hasło</Label>
                                <Col sm="9">
                                    <Input type="password" name="rpassword" id="repeatPassword" value={rpassword} error={errors.rpassword} onChange={this.onChange} />
                                    <span className="error">{errors.rpassword}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <div className="text-center">
                                <Button color="primary" type="submit">Zapisz</Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </section>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({setUser, state}) => {
            return <Index {...props} setUser={setUser} userData={state.user}  userToken={state.token} />
        }}
    </MyContext.Consumer>
)
