import React from 'react';
import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'

const numberMask = createNumberMask({
    prefix: '',
    suffix: '',
    includeThousandsSeparator: false,
    thousandsSeparatorSymbol: '',
    allowDecimal: true,
});

export default (props) => {
    return (
        <React.Fragment>
            <MaskedInput mask={numberMask} {...props} />
        </React.Fragment>
    );
}