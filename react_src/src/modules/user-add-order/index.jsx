import React from 'react';
import { Redirect } from 'react-router-dom';
import {Form, FormGroup, Row, Col, Label, Input, CustomInput, Button, Alert} from 'reactstrap';
import {validateInput} from '../validation/addOrder';
import Moment from 'moment';
import locutusEmpty from "locutus/php/var/empty";
import momentLocalizer from 'react-widgets-moment';
import axios from 'axios';
import PlacesAutocomplete from 'react-places-autocomplete';
import { MyContext } from "../../context";
import GWCommon from "../../core/common";
import FileInput from 'simple-react-file-uploader';
import Preloader from '../preloader/preloader';
import scrollToComponent from 'react-scroll-to-component';
import settings from '../../core/settings';
import provinces from '../../core/provinces';
import {MyUserContext} from "../../user-context";
import formPolyfillRun from 'formdata-polyfill';
import qs from "qs";
import GWDatePicker from "../datepicker";
import ExplainerItem from "./explainer";
import FloatInput from "./float_input";

if (GWCommon.IsSafariBrowser()) {
    window.FormData = undefined;
    formPolyfillRun();
} else {
    formPolyfillRun();
}

Moment.locale('pl');
momentLocalizer();

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ['OrdersForm[province]']: '',
            ['OrdersForm[city]']: '',
            ['OrdersForm[address]']: '',
            ['OrdersForm[surface]']: '',
            ['OrdersForm[end_date]']: '',
            ['OrdersForm[start_date]']: '',
            ['OrdersForm[description]']: '',
            ['OrdersForm[difficulty_level]']: '',
            ['OrdersForm[desription]']: '',
            photos: [],
            extraFiles: [],
            video: null,
            errors: {},
            isLoading: false,
            serwerErrorMsg: '',
            serwerSuccessMsg: '',
            categories_type: [],
        };
        this.myRef = React.createRef();
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.photosInputChange = this.photosInputChange.bind(this);
        this.photoFileInputChange = this.photoFileInputChange.bind(this);
        this.videoInputChange = this.videoInputChange.bind(this);
        this.extraFileInputChange = this.extraFileInputChange.bind(this);
        this.removePhoto = this.removePhoto.bind(this);
        this.geocode = this.geocode.bind(this);
        this.handleAutocompleteChange = this.handleAutocompleteChange.bind(this);
    }

    componentDidMount() {
        if (locutusEmpty(this.props.uncompletedAddForm) === false) {
            this.setState(this.props.uncompletedAddForm);
        }
    }

    handleAutocompleteChange(address) {
        const arr = address.split(",");
        this.setState({
            ['OrdersForm[city]']: arr[0].trim(),
        });
    }

    videoInputChange(arr) {
        if (arr.length > 0) {
            this.setState({
                video: arr[0],
            }, () => {
                this.props.setUncompletedAddForm(this.state);
            });

            return;
        }

        this.setState({
            video: null,
        }, () => {
            this.props.setUncompletedAddForm(this.state);
        });
    }

    photoFileInputChange(arr) {
        const sanitized = [];

        arr.map(el => {
            sanitized.push(el);
        });

        this.setState({
            photos: sanitized,
        }, () => {
            this.props.setUncompletedAddForm(this.state);
        });
    }

    extraFileInputChange(arr) {
        const sanitized = [];

        arr.map(el => {
            sanitized.push(el);
        });

        this.setState({
            extraFiles: sanitized,
        }, () => {
            this.props.setUncompletedAddForm(this.state);
        });
    }

    photosInputChange(photoData) {
        const photosArr = this.state.photos;
        photosArr.push(photoData);

        this.setState({
            photos: photosArr,
        });
    }

    removePhoto(id) {
        let currentPhotos = this.state.photos;
        currentPhotos.splice(id, 1);
        this.setState({
            photos: currentPhotos,
        });
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }
        return isValid;
    }
    onChange(e) {
        const target = e.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        const clearError = this.state.errors;
        delete clearError[name];
        this.setState({
            [name]: value,
            errors: clearError,
        }, () => {
            this.props.setUncompletedAddForm(this.state);
        });
    }
    geocode(province, city) {
        const nominatimBase = 'https://nominatim.openstreetmap.org/search';
        const querySting = qs.stringify({
            format: 'json',
            q: city + ', ' + province + ', Polska',
        });

        return new Promise((resolve) => {
            axios.get(`${nominatimBase}?${querySting}`).then((response) => {
                if (typeof response.data[0] !== 'undefined') {
                    return resolve({
                        lat: parseFloat(response.data[0].lat),
                        lng: parseFloat(response.data[0].lon),
                    });
                }

                return resolve({
                    lat: 51.7592485,
                    lng: 19.4559833,
                });
            })
                .catch(() => {
                    return resolve({
                        lat: 51.7592485,
                        lng: 19.4559833,
                    });
                });
        });
    }
    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            this.setState({
                isLoading: true,
            });
            const data = new FormData(e.target);

            this.geocode(this.state['OrdersForm[province]'], this.state['OrdersForm[city]']).then(geoData => {
                data.set('OrdersForm[lat]', geoData.lat.toString());
                data.set('OrdersForm[lng]', geoData.lng.toString());
                data.set('OrdersForm[city]', this.state['OrdersForm[city]']);
                data.set('OrdersForm[end_date]', this.state['OrdersForm[end_date]']);
                data.set('OrdersForm[start_date]', this.state['OrdersForm[start_date]']);
                if (this.state.video !== null) {
                    data.set('OrdersForm[video]', this.state.video, this.state.video.name);
                }
                if (this.state.photos.length > 0) {
                    this.state.photos.forEach((photoVal) => {
                        data.append('photos[]', photoVal, photoVal.name);
                    });
                }
                if (this.state.extraFiles.length > 0) {
                    this.state.extraFiles.forEach((fileVal) => {
                        data.append('otherfiles[]', fileVal, fileVal.name);
                    });
                }
                const instance = axios.create({
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': `Bearer ${this.props.userToken}`,
                    },
                });

                instance.post(`${settings.backendDomain}/orders/create`, data, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': `Bearer ${this.props.userToken}`,
                    },
                }).then((response) => {
                    if (response.data.success) {
                        this.setState({
                            serwerSuccessMsg: 'Zlecenie zostało dodane pomyślnie! Teraz należy czekać na zaakceptowanie zlecenia przez administrację. Dziękujemy za cierpliwość.',
                            errors: {},
                            serwerErrorMsg: '',
                            isLoading: false,
                        });
                        this.props.setSessionMessage(this.state.serwerSuccessMsg);
                        this.props.setUncompletedAddForm({});

                        scrollToComponent(this.myRef.current, {
                            align: 'top',
                            duration: 1,
                        });

                        return;
                    }

                    this.setState({
                        serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.',
                        isLoading: false,
                    });

                    scrollToComponent(this.myRef.current, {
                        align: 'top',
                        duration: 1,
                    });
                })
                    .catch(() => {
                        this.setState({
                            serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.',
                            isLoading: false,
                        });

                        scrollToComponent(this.myRef.current, {
                            align: 'top',
                            duration: 1,
                        });
                    });
            });
        }
    }

    render() {
        const {errors, isLoading, serwerErrorMsg, serwerSuccessMsg, categories_type } = this.state;

        if (serwerSuccessMsg.length > 0) {
            return <Redirect to="/moje-konto/zamowienia"/>;
        }

        return (
            <section className="userData" ref={this.myRef}>
                {
                    (isLoading === true) ?
                        <Preloader />
                        :null
                }
                <Form onSubmit={this.onSubmit}>
                    <Row>
                        <Col xs="12">
                            <h5>Zamówienie</h5>
                            {
                                (serwerErrorMsg) ? (
                                    <Alert color="danger">{serwerErrorMsg}</Alert>
                                ) : null
                            }
                            {
                                (serwerSuccessMsg) ? (
                                    <Alert color="success">{serwerSuccessMsg}</Alert>
                                ) : null
                            }
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <FormGroup>
                                <Label sm="12" for="orderProvince"><strong>Województwo</strong></Label>
                                <div className="col-sm-12">
                                    <CustomInput value={this.state['OrdersForm[province]']} type="select" id="orderProvince" name="OrdersForm[province]" onChange={this.onChange}>
                                        <option value="">Wybierz</option>
                                        {
                                            provinces.map((val) => (
                                                <option key={val} value={val}>{val}</option>
                                            ))
                                        }
                                    </CustomInput>
                                    <span className="error">{errors['OrdersForm[province]']}</span>
                                </div>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label for="orderPlace" sm="12"><strong>Miejscowość</strong></Label>
                                <Col sm="12">
                                    <PlacesAutocomplete
                                        value={this.state['OrdersForm[city]']}
                                        onChange={this.handleAutocompleteChange}
                                        searchOptions={
                                            {
                                                types: ['(cities)'],
                                                componentRestrictions: {country: 'pl'},
                                            }
                                        }
                                    >
                                        {({getInputProps, suggestions, getSuggestionItemProps, loading}) => (
                                            <div className="autocomplete-wrapper">
                                                <Input
                                                    {...getInputProps({
                                                        placeholder: 'Wybierz miasto..',
                                                        className: 'location-search-input',
                                                    })}
                                                />
                                                <div className="autocomplete-dropdown-container">
                                                    {loading && <div>Loading...</div>}
                                                    {suggestions.map(suggestion => {
                                                        const className = suggestion.active
                                                            ? 'suggestion-item--active'
                                                            : 'suggestion-item';
                                                        // inline style for demonstration purpose
                                                        const style = suggestion.active
                                                            ? {backgroundColor: '#fafafa', cursor: 'pointer'}
                                                            : {backgroundColor: '#ffffff', cursor: 'pointer'};
                                                        return (
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className,
                                                                    style,
                                                                })}
                                                            >
                                                                <span>{suggestion.formattedSuggestion.mainText}</span>
                                                            </div>
                                                        );
                                                    })}
                                                </div>
                                            </div>
                                        )}
                                    </PlacesAutocomplete>
                                    <span className="error">{errors['OrdersForm[city]']}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label for="orderSize" sm="12"><strong>Ulica i numer posesji</strong></Label>
                                <Col sm="12">
                                    <Input value={this.state['OrdersForm[address]']} name="OrdersForm[address]" id="orderAddress" onChange={this.onChange} />
                                    <span className="error">{errors['OrdersForm[address]']}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label for="orderSize" sm="12"><strong>Powierzchnia (w hektarach)</strong></Label>
                                <Col sm="12">
                                    <FloatInput value={this.state['OrdersForm[surface]']} name="OrdersForm[surface]" id="orderSize" onChange={this.onChange} className="form-control" />
                                    <span className="error">{errors['OrdersForm[surface]']}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label sm="12"><strong>Prace do wykonania</strong></Label>
                            </FormGroup>
                            {
                                this.props.industries.map((val, id) => (
                                    <FormGroup key={id} check>
                                        <Label check sm="12">
                                            <Input name="categories[]" checked={(categories_type.indexOf(val.id) > -1)} value={val.id} type="checkbox" onChange={(e) => {
                                                const target = e.target;

                                                if (target.checked) {
                                                    categories_type.push(target.value);
                                                } else {
                                                    const index = categories_type.indexOf(target.value);
                                                    if (index > -1) {
                                                        categories_type.splice(index, 1);
                                                    }
                                                }

                                                this.setState({
                                                    categories_type,
                                                });
                                            }} />{' '}
                                            {val.name}
                                        </Label>
                                    </FormGroup>
                                ))
                            }
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label sm="12" for="orderDifficulty"><strong>Poziom Trudności</strong></Label>
                                <div className="col-sm-12">
                                    <CustomInput type="select" value={this.state['OrdersForm[difficulty_level]']} id="orderDifficulty" name="OrdersForm[difficulty_level]" onChange={this.onChange}>
                                        <option value="">Wybierz</option>
                                        <option value="1">Bardzo Łatwy</option>
                                        <option value="2">Łatwy</option>
                                        <option value="3">Średni</option>
                                        <option value="4">Trudny</option>
                                        <option value="5">Bardzo Trudny</option>
                                        <option value="99">Nie wiem</option>
                                    </CustomInput>
                                    <span className="error">{errors['OrdersForm[difficulty_level]']}</span>
                                </div>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <h5 className="sub-title">Pytania dodatkowe</h5>
                        </Col>
                        {
                            (categories_type.length === 0) ?
                                <Col xs="12">
                                    <p className="col-12 col-form-label">Nie wybrałeś żadnej pracy do wykonania</p>
                                </Col> :
                                <React.Fragment>
                                    {
                                        this.props.industries.map((val, id) => (
                                            <Col xs="12" key={id} id={val.key} style={{
                                                display: (categories_type.indexOf(val.id) === -1) ? 'none' : '',
                                            }}>
                                                <FormGroup>
                                                    {
                                                        (val.questions.length > 0) ?
                                                            <Label className="highlight" sm="12">{val.name}:</Label>
                                                            : null
                                                    }
                                                    {
                                                        val.questions.map((q, qid) => {
                                                            switch (q.type) {
                                                            case 'text':
                                                                return (
                                                                    <React.Fragment key={qid}>
                                                                        <Col sm="12" className="section">
                                                                            <Label
                                                                                for={`${q.id}-${q.key}-${val.key}-input`}>
                                                                                <strong>{q.question}</strong>
                                                                                {(!locutusEmpty(q.tooltip)) ? <ExplainerItem text={q.tooltip} /> : null}
                                                                            </Label>
                                                                            <Input type="text" value={(this.state[`extraData[${q.id}-${q.key}]`]) ? this.state[`extraData[${q.id}-${q.key}]`] : ''} name={`extraData[${q.id}-${q.key}]`} id={`${q.id}-${q.key}-${val.key}-input`} onChange={this.onChange}/>
                                                                        </Col>
                                                                    </React.Fragment>
                                                                );
                                                                break;
                                                            case 'number':
                                                                return (
                                                                    <React.Fragment key={qid}>
                                                                        <Col sm="12" className="section">
                                                                            <Label
                                                                                for={`${q.id}-${q.key}-${val.key}-input`}>
                                                                                <strong>{q.question}</strong>
                                                                                {(!locutusEmpty(q.tooltip)) ? <ExplainerItem text={q.tooltip} /> : null}
                                                                            </Label>
                                                                            <Input type="number" value={(this.state[`extraData[${q.id}-${q.key}]`]) ? this.state[`extraData[${q.id}-${q.key}]`] : ''} name={`extraData[${q.id}-${q.key}]`} id={`${q.id}-${q.key}-${val.key}-input`} onChange={this.onChange}/>
                                                                        </Col>
                                                                    </React.Fragment>
                                                                );
                                                                break;

                                                            case 'radio':
                                                                return (
                                                                    <React.Fragment key={qid}>
                                                                        <Col sm="12" className="section">
                                                                            <Label sm="12">
                                                                                <strong>{q.question}</strong>
                                                                                {(!locutusEmpty(q.tooltip)) ? <ExplainerItem text={q.tooltip} /> : null}
                                                                            </Label>
                                                                            {
                                                                                q.available_answers.split(',').map((avail, availid) => (
                                                                                    <FormGroup check key={availid}>
                                                                                        <Label check sm="12">
                                                                                            <Input type="radio" checked={(this.state[`extraData[${q.id}-${q.key}]`] === avail)} value={avail} name={`extraData[${q.id}-${q.key}]`} onChange={this.onChange}/>{' '}
                                                                                            {avail}
                                                                                        </Label>
                                                                                    </FormGroup>
                                                                                ))
                                                                            }
                                                                        </Col>
                                                                    </React.Fragment>
                                                                );
                                                                break;

                                                            case 'checkbox':
                                                                return (
                                                                    <React.Fragment key={qid}>
                                                                        <Col sm="12" className="section">
                                                                            <Label sm="12">
                                                                                <strong>{q.question}</strong>
                                                                                {(!locutusEmpty(q.tooltip)) ? <ExplainerItem text={q.tooltip} /> : null}
                                                                            </Label>
                                                                            {
                                                                                q.available_answers.split(',').map((avail, availid) => (
                                                                                    <FormGroup check key={availid}>
                                                                                        <Label check sm="12">
                                                                                            <Input type="checkbox" value={avail} name={`extraData[${q.id}-${q.key}][]`}/>{' '}
                                                                                            {avail}
                                                                                        </Label>
                                                                                    </FormGroup>
                                                                                ))
                                                                            }
                                                                        </Col>
                                                                    </React.Fragment>
                                                                );
                                                                break;
                                                            default:
                                                                return '';
                                                            }
                                                        })
                                                    }
                                                </FormGroup>
                                            </Col>
                                        ))
                                    }
                                </React.Fragment>
                        }
                        <Col xs="12">
                            <h5 className="sub-title">Pozostałe informacje</h5>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label for="orderDateStart" sm="12"><strong>Oczekiwany termin rozpoczęcia prac</strong></Label>
                                <Col sm="12">
                                    <GWDatePicker
                                        value={this.state['OrdersForm[start_date]']}
                                        placeholder="Kliknij by wybrać datę"
                                        onChange={value => {
                                            const dateEnd = this.state.errors;
                                            delete dateEnd['OrdersForm[start_date]'];
                                            this.setState({
                                                ['OrdersForm[start_date]']: value,
                                                errors: dateEnd,
                                            }, () => {
                                                this.props.setUncompletedAddForm(this.state);
                                            });
                                        } }
                                    />
                                    <span className="error">{errors['OrdersForm[start_date]']}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label for="orderDateEnd" sm="12"><strong>Oczekiwany termin zakończenia prac</strong></Label>
                                <Col sm="12">
                                    <GWDatePicker
                                        value={this.state['OrdersForm[end_date]']}
                                        placeholder="Kliknij by wybrać datę"
                                        onChange={value => {
                                            const dateEnd = this.state.errors;
                                            delete dateEnd['OrdersForm[end_date]'];
                                            this.setState({
                                                ['OrdersForm[end_date]']: value,
                                                errors: dateEnd,
                                            }, () => {
                                                this.props.setUncompletedAddForm(this.state);
                                            });
                                        } }
                                    />
                                    <span className="error">{errors['OrdersForm[end_date]']}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label for="orderThing" sm="12"><strong>Opis i uwagi</strong></Label>
                                <Col sm="12">
                                    <Input type="textarea" value={this.state['OrdersForm[description]']} name="OrdersForm[description]" id="orderThing" onChange={this.onChange} />
                                    <span className="error">{errors['OrdersForm[description]']}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label sm="12"><strong>Zdjęcia</strong></Label>
                                <Col sm="12">
                                    <FileInput
                                        multiple={true}
                                        onChange = {this.photoFileInputChange}
                                        accept="image/*"
                                        className="custom"
                                    />
                                </Col>
                                {/*{
                                    photos.map((val, id) => (
                                        <div key={id} className='single-uploaded-photo'>
                                            <img src={`${settings.backendDomain}${val.display}`} />
                                            <br />
                                            <Button color="danger" onClick={() => this.removePhoto(id)}>Usuń zjęcie</Button>
                                        </div>
                                    ))
                                }
                                <ImageCropperModule pushPhoto={this.photosInputChange} />*/}
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label for="orderVideo" sm="12"><strong>Film</strong></Label>
                                <Col sm="12">
                                    <FileInput
                                        multiple={false}
                                        onChange = {this.videoInputChange}
                                        accept="video/*"
                                        className="custom"
                                    />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup>
                                <Label sm="12"><strong>Pliki</strong></Label>
                                <Col sm="12">
                                    <FileInput
                                        multiple={true}
                                        onChange = {this.extraFileInputChange}
                                        accept="*"
                                        className="custom"
                                    />
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <div className="text-center">
                                <Button color="primary" right="true">Zapisz</Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </section>
        );
    }
}

const UserContextWrapper = props => (
    <MyUserContext.Consumer>
        {({state, setUncompletedAddForm}) => {
            return <Index {...props} uncompletedAddForm={state.uncompletedAddForm} setUncompletedAddForm={setUncompletedAddForm} />
        }}
    </MyUserContext.Consumer>
);

export default props => (
    <MyContext.Consumer>
        {({state, setSessionMessage}) => {
            return <UserContextWrapper {...props} industries={state.industries} userToken={state.token} setSessionMessage={setSessionMessage} />
        }}
    </MyContext.Consumer>
);
