import React from 'react';
import FontAwesome from 'react-fontawesome';
import { MyContext } from "../../context";
import Listing from "../list-group/listing";
import axios from 'axios';
import settings from '../../core/settings';
import PopupError from "../../modules/popup/popup_error";
import locutusEmpty from "locutus/php/var/empty";

class FollowButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listingData: [],
            renderError: false,
        };
        this.followOrder = this.followOrder.bind(this);
        this.getListing = this.getListing.bind(this);
        this.abortRequest = this.abortRequest.bind(this);
    }

    componentDidMount() {
        if (this.props.userToken.length > 0) {
            if (locutusEmpty(this.props.userData.role) === false && this.props.userData.role !== 'customer' && !this.props.button) {
                this.getListing();
            }
        }
    }

    abortRequest() {
        if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
            this.serverRequest.abort();
        }
    }

    getListing() {
        const getOrdersWatchedUrl = `${settings.backendDomain}/orders/watched`;
        const instance = axios.create({
            headers: {'Authorization': `Bearer ${this.props.userToken}`},
        });
        this.serverRequest = instance.get(getOrdersWatchedUrl, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
            .then(response => {
                this.setState({
                    renderError: false,
                    listingData: response.data.listing,
                });
            }).catch((error) => {
                this.setState({
                    renderError: true,
                });
            });
    }

    componentWillUnmount() {
        this.abortRequest();
    }

    componentWillReceiveProps(nextProps) {
        this.abortRequest();

        if (nextProps.userToken.length > 0) {
            if (nextProps.userData.role !== 'customer' && !nextProps.button) {
                this.getListing();
            }
        }
    }

    followOrder(e){
        e.preventDefault();

        const postOrdersWatchedUrl = `${settings.backendDomain}/orders/watched/${this.props.orderId}`;
        const instance = axios.create({
            headers: {'Authorization': `Bearer ${this.props.userToken}`},
        });
        if (this.props.watchedByMe === false) {
            instance.post(postOrdersWatchedUrl, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                .then(response => {
                    this.props.updateWatched(true);
                    this.setState({
                        renderError: false,
                    });
                }).catch((error) => {
                    this.setState({
                        renderError: true,
                    });
                });
        } else {
            instance.delete(postOrdersWatchedUrl, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                .then(response => {
                    this.props.updateWatched(false);
                    this.setState({
                        renderError: false,
                    });
                }).catch((error) => {
                    this.setState({
                        renderError: true,
                    });
                });
        }

    }

    render() {
        if (this.state.renderError) {
            return (
                <PopupError isOpen={true} title="Wystąpił błąd"><p>Wystąpił błąd aplikacji, należy odświeżyć stronę. Jeśli problem nie ustąpi prosimy o kontakt z administracją</p></PopupError>
            );
        }

        return (
            <React.Fragment>
                {
                    (this.props.button) ?
                        <React.Fragment>
                            <a href="#" className="btn btn-primary add-follow ml-auto" onClick={this.followOrder}>
                                {
                                    (this.props.watchedByMe === false) ?
                                        <React.Fragment>
                                            <FontAwesome name="eye" /> Dodaj do obserwowanych
                                        </React.Fragment> :
                                        <React.Fragment>
                                            <FontAwesome name="eye-slash" /> Usuń z obserwowanych
                                        </React.Fragment>
                                }
                            </a>
                            <div className="clearfix"></div>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <Listing data={this.state.listingData} />
                        </React.Fragment>
                }
            </React.Fragment>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <FollowButton {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)
