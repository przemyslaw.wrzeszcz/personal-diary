import React from 'react';
import {Col, Form, FormGroup, Label} from 'reactstrap';
import PopupConfirm from "../popup/popup_confirm";
import GWDatePicker from "../datepicker";

export default ({showRecreate, setRecreate, okFN, errors, inputChange}) => {
    return (
        <PopupConfirm isOpen={showRecreate} toggle={() => {setRecreate(false);}} title="Wystaw ponownie" okLabel="Wystaw" okFN={okFN}>
            <p>Wybierz nowe daty dla zlecenia.</p>
            <Form onSubmit={(e) => {e.preventDefault();}}>
                <FormGroup>
                    <Label for="orderDateStart" sm="12">Oczekiwany termin rozpoczęcia prac</Label>
                    <Col sm="12">
                        <GWDatePicker
                            placeholder="Kliknij by wybrać datę"
                            onChange={value => {
                                const dateEnd = errors;
                                delete dateEnd['OrdersForm[start_date]'];
                                inputChange('start_date', value, dateEnd);
                            } }
                        />
                        <span className="error">{errors['OrdersForm[start_date]']}</span>
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Label for="orderDateEnd" sm="12">Oczekiwany termin zakończenia prac</Label>
                    <Col sm="12">
                        <GWDatePicker
                            placeholder="Kliknij by wybrać datę"
                            onChange={value => {
                                const dateEnd = errors;
                                delete dateEnd['OrdersForm[end_date]'];
                                inputChange('end_date', value, dateEnd);
                            } }
                        />
                        <span className="error">{errors['OrdersForm[end_date]']}</span>
                    </Col>
                </FormGroup>
            </Form>
        </PopupConfirm>
    )

}
