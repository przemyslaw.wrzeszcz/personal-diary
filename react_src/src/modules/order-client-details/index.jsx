import React from 'react';
import { Table, Input, Button, Form, FormGroup, Alert, Label, InputGroup } from 'reactstrap';
import PopupConfirm from "../popup/popup_confirm";
import axios from "axios";
import settings from "../../core/settings";
import GWCommon from "../../core/common";
import {MyContext} from "../../context";
import Preloader from "../preloader/preloader";
import {validateInput} from "../validation/recreateOrder";
import Recreate from "./recreate";
import formPolyfillRun from 'formdata-polyfill';
import {Redirect} from "react-router-dom";

if (GWCommon.IsSafariBrowser()) {
    window.FormData = undefined;
    formPolyfillRun();
} else {
    formPolyfillRun();
}


class OrderClientDetails extends React.Component {
    constructor() {
        super();
        this.state = {
            choosenOffer: 0,
            showConfirm: false,
            showLoader: false,
            serwerErrorMsg: '',
            showRecreate: false,
            recreateSuccess: false,
            ['OrdersForm[end_date]']: '',
            ['OrdersForm[start_date]']: '',
            errors: {},
        };
        this.onChange = this.onChange.bind(this);
        this.showConfirm = this.showConfirm.bind(this);
        this.finishOrder = this.finishOrder.bind(this);
        this.validateSelection = this.validateSelection.bind(this);
        this.isValid = this.isValid.bind(this);
        this.recreateOrder = this.recreateOrder.bind(this);
        this.setRecreateState = this.setRecreateState.bind(this);
        this.setInputAndErrors = this.setInputAndErrors.bind(this);
        this.renderOfferSelect = this.renderOfferSelect.bind(this);
        this.renderClosedOrder = this.renderClosedOrder.bind(this);
    }

    onChange(e) {
        const target = e.target;
        const value = target.value;

        this.setState({
            choosenOffer: value,
            serwerErrorMsg: '',
        });
    }

    showConfirm() {
        this.setState({
            showConfirm: !this.state.showConfirm,
        });
    }

    validateSelection() {
        if (this.state.choosenOffer === 0) {
            this.setState({
                serwerErrorMsg: 'Zaznacz zwycięską ofertę',
            });
        } else {
            this.showConfirm();
        }
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }
        return isValid;
    }

    setRecreateState(val) {
        this.setState({showRecreate: val})
    }

    setInputAndErrors(key, value, errors) {
        this.setState({
            [`OrdersForm[${key}]`]: value,
            errors: errors,
        });
    }

    recreateOrder() {
        if (this.isValid()) {
            this.setState({
                showLoader: true,
                showRecreate: false,
            });
            const data = new FormData();
            data.set('OrdersForm[end_date]', this.state['OrdersForm[end_date]']);
            data.set('OrdersForm[start_date]', this.state['OrdersForm[start_date]']);

            const instance = axios.create({
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${this.props.userToken}`,
                },
            });

            instance.post(`${settings.backendDomain}/orders/recreate/${this.props.orderId}`, data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${this.props.userToken}`,
                },
            }).then((response) => {
                if (response.data.success) {
                    this.setState({
                        errors: {},
                        serwerErrorMsg: '',
                        showLoader: false,
                        recreateSuccess: true,
                    });
                    this.props.setSessionMessage('Zlecenie zostało dodane pomyślnie! Teraz należy czekać na zaakceptowanie zlecenia przez administrację. Dziękujemy za cierpliwość.');

                    return;
                }

                this.setState({
                    serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.',
                    showLoader: false,
                });
            })
                .catch(() => {
                    this.setState({
                        serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.',
                        showLoader: false,
                    });
                });
        }
    }

    finishOrder() {
        const instance = axios.create({
            headers: {'Authorization': `Bearer ${this.props.userToken}`},
        });
        this.setState({
            showConfirm: false,
            showLoader: true,
        });

        instance.post(`${settings.backendDomain}/orders/${this.props.orderId}`, {choosenOffer: this.state.choosenOffer}, {headers: {'Authorization': `Bearer ${this.props.userToken}`}}).then((response) => {
            if (response.data.success) {
                this.props.selectWinner(this.state.choosenOffer);
                this.setState({
                    showConfirm: false,
                    showLoader: false,
                });

                return;
            }

            this.setState({
                showConfirm: false,
                showLoader: false,
                serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie lub odświeżyć stronę.',
            });
        })
            .catch((error) => {
                this.setState({
                    showConfirm: false,
                    showLoader: false,
                    serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.',
                });
            });
    }

    renderClosedOrder() {
        let winner = {};

        for (let i = 0; i < this.props.offers.length; i++) {
            if (parseInt(this.props.offers[i].selected) === 1) {
                winner = this.props.offers[i];
                break;
            }
        }

        return (
            <aside className="offer-window section-background">
                {
                    (this.state.showLoader) ? (
                        <Preloader />
                    ) : null
                }
                {
                    (this.state.serwerErrorMsg) ? (
                        <Alert color="danger">{this.state.serwerErrorMsg}</Alert>
                    ) : null
                }
                <FormGroup>
                    <Alert className="text-center" color="primary">Wybrana oferta:</Alert>
                    <Label className="text-center">Cena:</Label>
                    <p className="text-center">{winner.price}</p>
                </FormGroup>
                <hr/>
                <p className="text-center">Proponowany termin realizacji:</p>
                <div className="inlineForm">
                    <FormGroup>
                        <Label className="faded">TERMIN ROZPOCZĘCIA:</Label>
                        <InputGroup>
                            <p className="text-center">{winner.start_date}</p>
                        </InputGroup>
                    </FormGroup>
                    <FormGroup>
                        <Label className="faded">TERMIN ZAKOŃCZENIA:</Label>
                        <InputGroup>
                            <p className="text-center">{winner.end_date}</p>
                        </InputGroup>
                    </FormGroup>
                </div>
                <Recreate
                    overrideShow
                    showRecreate={this.state.showRecreate}
                    setRecreate={this.setRecreateState}
                    okFN={this.recreateOrder}
                    errors={this.state.errors}
                    inputChange={this.setInputAndErrors} />
                <div className="clearfix"></div>
            </aside>
        );
    }

    renderOfferSelect() {
        return (
            <aside className="order-client-details section-background">
                <PopupConfirm isOpen={this.state.showConfirm} toggle={this.showConfirm} title="Potwierdź zamknięcie zlecenia" okLabel="Potwierdzam" okFN={this.finishOrder}>
                    <p>Czy na pewno chcesz wybrać zwycięską ofertę i zamknąć to zlecenie? Tej operacji nie można cofnąć.</p>
                </PopupConfirm>
                {
                    (this.state.showLoader) ? (
                        <Preloader />
                    ) : null
                }
                {
                    (this.state.serwerErrorMsg) ? (
                        <Alert color="danger">{this.state.serwerErrorMsg}</Alert>
                    ) : null
                }
                <Form>
                    <Table size="sm" responsive>
                        <thead>
                            <tr>
                                <th>lp.</th>
                                <th>Termin rozpoczęcia prac</th>
                                <th>Termin zakończenia prac</th>
                                <th>Cena</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.offers.map((val, id) =>
                                    <tr key={id}>
                                        <td>{id+1}.</td>
                                        <td>{val.start_date}</td>
                                        <td>{val.end_date}</td>
                                        <td>{val.price} zł</td>
                                        <td><Input type="radio" name="selectedWinner" value={val.id} onChange={this.onChange} /></td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </Table>
                    <Button className="left" color="primary" onClick={this.validateSelection}>Wybierz Zwycięzcę</Button>
                    <Recreate
                        deadline={this.props.deadline}
                        showRecreate={this.state.showRecreate}
                        setRecreate={this.setRecreateState}
                        okFN={this.recreateOrder}
                        errors={this.state.errors}
                        inputChange={this.setInputAndErrors} />
                    <div className="clearfix"></div>
                </Form>
            </aside>
        );
    }

    render() {
        if (this.state.recreateSuccess === true) {
            return <Redirect to="/moje-konto/zamowienia"/>;
        }

        let noOffers = !Array.isArray(this.props.offers);

        if (!noOffers) {
            noOffers = this.props.offers.length  <= 0
        }

        if (noOffers) {
            return (
                <aside className="order-client-details section-background">
                    <FormGroup>
                        {
                            (this.state.showLoader) ? (
                                <Preloader />
                            ) : null
                        }
                        {
                            (this.state.serwerErrorMsg) ? (
                                <Alert color="danger">{this.state.serwerErrorMsg}</Alert>
                            ) : null
                        }
                        <Alert color="info">Nie zostały jeszcze złożone żadne oferty do tego zlecenia.</Alert>
                        {/*<Button color="primary">Edytuj zlecenie</Button>*/}
                        <Recreate
                            deadline={this.props.deadline}
                            showRecreate={this.state.showRecreate}
                            setRecreate={this.setRecreateState}
                            okFN={this.recreateOrder}
                            errors={this.state.errors}
                            inputChange={this.setInputAndErrors} />
                        <div className="clearfix"></div>
                    </FormGroup>
                </aside>
            );
        }

        if (parseInt(this.props.closedOrder) !== 0) {
            return this.renderClosedOrder();
        }

        return this.renderOfferSelect();
    }
}


export default props => (
    <MyContext.Consumer>
        {({state, setSessionMessage}) => {
            return <OrderClientDetails {...props} setSessionMessage={setSessionMessage} userData={state.user} userToken={state.token} />
        }}
    </MyContext.Consumer>
)
