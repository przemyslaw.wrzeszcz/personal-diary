import React from 'react';
import {Button} from 'reactstrap';
import ConfirmRecreate from "./confirm-recreate";
import FontAwesome from 'react-fontawesome';
import GWCommon from "../../core/common";
import locutusEmpty from "locutus/php/var/empty";

export default ({overrideShow, deadline, showRecreate, setRecreate, okFN, errors, inputChange}) => {
    const duration = GWCommon.calcDateDiffInDays(deadline);

    if (duration >= 0 && locutusEmpty(overrideShow)) {
        return null;
    }

    return (
        <React.Fragment>
            <ConfirmRecreate showRecreate={showRecreate} setRecreate={setRecreate} okFN={okFN} errors={errors} inputChange={inputChange} />
            <Button className="right" color="secondary" onClick={() => {setRecreate(true);}}><FontAwesome name="redo" /> Wystaw ponownie</Button>
        </React.Fragment>
    )

}
