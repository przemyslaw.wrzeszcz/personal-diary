import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import MyListItem from '../list-group/item';

export default class PaginationMod extends React.Component {
    render() {
        const { pageNumbers, currentPage, itemClick } = this.props;

        const renderPageNumbers = pageNumbers.map(number => {
            return (
                <PaginationItem key={number} active={(number === currentPage)}>
                    <PaginationLink
                        id={number}
                        onClick={itemClick}
                    >
                        {number}
                    </PaginationLink>
                </PaginationItem>
            );
        });

        return (
            <React.Fragment>
                <Pagination id="page-numbers">
                    {renderPageNumbers}
                </Pagination>
            </React.Fragment>
        );
    }
}