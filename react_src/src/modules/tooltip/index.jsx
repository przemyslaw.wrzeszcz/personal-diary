import React from 'react';
import { Button, Tooltip } from 'reactstrap';

export default class TooltipItem extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            tooltipOpen: false
        };
    }

    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }

    render() {
        return (
            <span>
                <a href="#" className="tooltip-trigger" id={'Tooltip-' + this.props.id}>
                    i
                </a>
                <Tooltip placement={this.props.placement} isOpen={this.state.tooltipOpen} target={'Tooltip-' + this.props.id} toggle={this.toggle}>
                    {this.props.text}
                </Tooltip>
            </span>
        );
    }
}