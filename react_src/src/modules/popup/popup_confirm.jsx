import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class PopupConfirm extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} className={this.props.className}>
                    <ModalHeader toggle={undefined}>{this.props.title}</ModalHeader>
                    <ModalBody>
                        {this.props.children}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.props.okFN}>{this.props.okLabel}</Button>
                        <Button color="danger" onClick={this.props.toggle}>Zamknij</Button>
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        );
    }
}
