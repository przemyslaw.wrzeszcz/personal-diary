import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

export default class PopupError extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Modal isOpen={this.props.isOpen} className="error">
                    <ModalHeader>{this.props.title}</ModalHeader>
                    <ModalBody>
                        {this.props.children}
                    </ModalBody>
                </Modal>
            </React.Fragment>
        );
    }
}
