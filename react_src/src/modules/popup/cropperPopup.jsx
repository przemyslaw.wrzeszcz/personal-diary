import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class Cropup extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>{this.props.title}</ModalHeader>
                    <ModalBody>
                        {this.props.children}
                    </ModalBody>
                    <ModalFooter>
                        {
                            (this.props.showSave === true) ?
                                <Button color="primary" onClick={this.props.saveFN}>Zapisz</Button>
                                :null
                        }
                        <Button color="danger" onClick={this.props.toggle}>Zamknij</Button>
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        );
    }
}
