import React from 'react';
import axios from 'axios';
import qs from 'qs';
import Popup from './popup';
import Preloader from './../preloader/preloader';
import { Map, Marker, TileLayer } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import scrollToComponent from "react-scroll-to-component";

export default class setLocMap extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            lat: 51.7592485,
            lng: 19.4559833,
            defaultCenter: {lat: 51.7592485, lng: 19.4559833},
            geocodeInProgress: false,
            geocodeCompleted: false,
            draggable: true,
        };

        this.refmarker = React.createRef();
        this.geocode = this.geocode.bind(this);
        this.updatePosition = this.updatePosition.bind(this);
    }

    geocode(province, city) {
        this.setState({
            geocodeInProgress: true,
        });

        const nominatimBase = 'https://nominatim.openstreetmap.org/search';
        const querySting = qs.stringify({
            format: 'json',
            q: city + ', ' + province + ', Polska',
        });

        axios.get(`${nominatimBase}?${querySting}`).then((response) => {
            if (typeof response.data[0] !== 'undefined') {
                this.props.setLatLng(
                    parseFloat(response.data[0].lat),
                    parseFloat(response.data[0].lon)
                );

                return this.setState({
                    lat: parseFloat(response.data[0].lat),
                    lng: parseFloat(response.data[0].lon),
                    defaultCenter: {lat: parseFloat(response.data[0].lat), lng: parseFloat(response.data[0].lon)},
                    geocodeCompleted: true,
                    geocodeInProgress: false,
                });
            }

            this.props.setLatLng(
                51.7592485,
                19.4559833
            );

            this.setState({
                lat: 51.7592485,
                lng: 19.4559833,
                defaultCenter: {lat: 51.7592485, lng: 19.4559833},
                geocodeCompleted: true,
                geocodeInProgress: false,
            });
        })
            .catch(() => {
                this.props.setLatLng(
                    51.7592485,
                    19.4559833
                );

                this.setState({
                    lat: 51.7592485,
                    lng: 19.4559833,
                    defaultCenter: {lat: 51.7592485, lng: 19.4559833},
                    geocodeCompleted: true,
                    geocodeInProgress: false,
                });
            });
    }

    componentWillMount() {
        const toSet = {
            lat: this.props.lat,
            lng: this.props.lng,
            geocodeCompleted: true,
            defaultCenter: {lat: this.props.lat, lng: this.props.lng},
        };

        if (toSet.lat !== 0 || toSet.lng !== 0) {
            this.setState(toSet);
        }
    }

    componentDidMount() {
        if (!this.state.geocodeInProgress && this.props.isOpen && !this.state.geocodeCompleted) {
            this.geocode(this.props.province, this.props.city);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.province !== nextProps.province || this.props.city !== nextProps.city) {
            this.setState({
                geocodeCompleted: false,
            });
        }
    }

    componentDidUpdate() {
        if (!this.state.geocodeInProgress && this.props.isOpen && !this.state.geocodeCompleted) {
            this.geocode(this.props.province, this.props.city);
        }
    }

    updatePosition() {
        const { lat, lng } = this.refmarker.current.leafletElement.getLatLng();
        this.props.setLatLng(
            lat,
            lng
        );
        this.setState({
            lat: lat,
            lng: lng,
        });
    }

    render() {
        if (this.props.isOpen !== true) {
            return '';
        }

        if (!this.state.geocodeCompleted) {
            return (
                <Preloader />
            )
        }

        return (
            <React.Fragment>
                <Popup className="mapHolder" title="Lokalizacja" toggle={this.props.toggle} isOpen={this.props.isOpen}>
                    <Map
                        zoom={10}
                        center={ this.state.defaultCenter }
                    >
                        <TileLayer
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                        />
                        <Marker
                            draggable={this.state.draggable}
                            onDragend={this.updatePosition}
                            position={{lat: this.state.lat, lng: this.state.lng}}
                            ref={this.refmarker}
                        />
                    </Map>
                    <p>Przeciągnij znacznik na rzeczywistą lokalizację zlecenia, jeśli ikona wskazuje błędnie.</p>
                </Popup>
            </React.Fragment>
        );
    }
}
