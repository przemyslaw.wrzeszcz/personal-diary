import React from 'react';
import { Form, FormGroup, Label, Input, Button, CustomInput, Alert } from 'reactstrap';
import {Link} from 'react-router-dom';
import {validateInput}  from '../validation/register';
import axios from 'axios';
import scrollToComponent from 'react-scroll-to-component';
import { MyContext } from "../../context";
import settings from '../../core/settings';

class RegisterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            acctype: '',
            name: '',
            surname: '',
            nip: '',
            phone: '',
            email: '',
            password: '',
            rpassword: '',
            agreement: false,
            errors: {},
            serwerErrorMsg: '',
            isLoading: false,
            serwerSuccessMsg: '',
            refPassed: false,
        };
        this.myRef = React.createRef();
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }
        return isValid;
    }

    onChange(e) {
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value,
        });
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            const registerApiUrl = `${settings.backendDomain}/users/register`;
            const payload={
                "role": this.state.acctype,
                "name": this.state.name,
                "surname": this.state.surname,
                "nip": this.state.nip,
                "phone": this.state.phone,
                "mail": this.state.email,
                "password": this.state.password,
                "rpassword": this.state.rpassword,
                "agreement": this.state.agreement,
            };
            axios.post(registerApiUrl, payload)
                .then((response) => {
                    if (response.data.success) {
                        this.setState({
                            serwerSuccessMsg: 'Rejestracja przebiegła pomyślnie. Twoje konto czeka na walidację. Poinformujemy Cię drogą mailową o jego aktywowaniu.',
                            acctype: '',
                            name: '',
                            surname: '',
                            nip: '',
                            phone: '',
                            email: '',
                            password: '',
                            rpassword: '',
                            agreement: false,
                            errors: {},
                            serwerErrorMsg: '',
                        });

                        scrollToComponent(this.myRef.current, {
                            align: 'top',
                            duration: 1,
                        });

                        return;
                    }

                    const errorMaping = {
                        "role": 'acctype',
                        "name": 'name',
                        "surname": 'surname',
                        "nip": 'nip',
                        "phone": 'phone',
                        "mail": 'email',
                        "password": 'password',
                        "rpassword": 'rpassword',
                        "agreement": 'agreement',
                    };
                    const errors = {};
                    Object.keys(response.data.errors).forEach((key) => {
                        errors[errorMaping[key]] = response.data.errors[key];
                    });
                    this.setState({ errors });
                })
                .catch((error) => {
                    this.setState({ serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.' });

                    scrollToComponent(this.myRef.current, {
                        align: 'top',
                        duration: 1,
                    });
                    // jak tutaj wpadamy to pokaż userowi komunikat że wystąpił nieoczekiwany błąd i niech spróbuje ponownie
                });
        }
    }
    render() {
        const { errors, acctype, name, surname, nip, phone, email, password, rpassword, agreement, isLoading, serwerErrorMsg, serwerSuccessMsg } = this.state;
        return (
            <section className="registerForm" ref={this.myRef}>
                <Form onSubmit={this.onSubmit}>
                    <h1 className="text-center">Rejestracja</h1>
                    {
                        (serwerErrorMsg) ? (
                            <Alert color="danger">{serwerErrorMsg}</Alert>
                        ) : null
                    }
                    {
                        (serwerSuccessMsg) ? (
                            <Alert color="success">{serwerSuccessMsg}</Alert>
                        ) : null
                    }
                    <FormGroup>
                        <Label for="racctype">Typ Konta<span className="required"> *</span></Label>
                        <CustomInput type="select" value={acctype} id="acctype" name="acctype" onChange={this.onChange}>
                            <option value="">---typ konta---</option>
                            <option value="customer">Klient</option>
                            <option value="executor">Wykonawca</option>
                        </CustomInput>
                        <span className="error">{errors.acctype}</span>
                    </FormGroup>
                    <FormGroup>
                        <Label for="registerName">Imię<span className="required"> *</span></Label>
                        <Input type="text" name="name" id="registerName" placeholder="Imię" value={name} error={errors.name} onChange={this.onChange} />
                        <span className="error">{errors.name}</span>
                    </FormGroup>
                    <FormGroup>
                        <Label for="registerSurname">Nazwisko<span className="required"> *</span></Label>
                        <Input type="text" name="surname" id="registerSurname" placeholder="Nazwisko" value={surname} error={errors.surname} onChange={this.onChange} />
                        <span className="error">{errors.surname}</span>
                    </FormGroup>
                    {
                        acctype === 'executor' ?
                            <FormGroup>
                                <Label for="registerNip">NIP<span className="required"> *</span></Label>
                                <Input type="number" name="nip" id="registerNip" placeholder="NIP" value={nip} error={errors.nip} onChange={this.onChange} />
                                <span className="error">{errors.nip}</span>
                            </FormGroup>: null
                    }
                    <FormGroup>
                        <Label for="registerPhone">Telefon<span className="required"> *</span></Label>
                        <Input type="number" name="phone" id="registerPhone" placeholder="Telefon" value={phone} error={errors.phone} onChange={this.onChange} />
                        <span className="error">{errors.phone}</span>
                    </FormGroup>
                    <FormGroup>
                        <Label for="registerEmail">Email<span className="required"> *</span></Label>
                        <Input type="email" name="email" id="registerEmail" placeholder="Email" value={email} error={errors.email} onChange={this.onChange} />
                        <span className="error">{errors.email}</span>
                    </FormGroup>
                    <FormGroup>
                        <Label for="registerPassword">Hasło<span className="required"> *</span></Label>
                        <Input type="password" name="password" id="registerPassword" placeholder="Hasło" value={password} error={errors.password} onChange={this.onChange} />
                        <span className="error">{errors.password}</span>
                    </FormGroup>
                    <FormGroup>
                        <Label for="regRePassword">Powtórz Hasło<span className="required"> *</span></Label>
                        <Input type="password" name="rpassword" id="regRePassword" placeholder="Powtórz hasło" value={rpassword} error={errors.rpassword} onChange={this.onChange} />
                        <span className="error">{errors.rpassword}</span>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input type="checkbox" name="agreement" onChange={this.onChange} value="1" checked={agreement} />{' '}
                            Zgadzam się z regulaminem i polityką prywatności<span className="required"> *</span>
                            <span className="error">{errors.agreement}</span>
                        </Label>
                    </FormGroup>
                    <span className="required">* pola wymagane <br/></span>
                    <Button color="primary" size="lg" block type="submit">Zarejestruj się</Button>
                    <br/>
                    <Label>Posiadasz już konto?</Label>
                    <Link exact="true" to="/logowanie" className="btn btn-secondary btn-lg btn-block">Zaloguj się</Link>
                </Form>
            </section>
        )
    }
}

export default props => (
    <MyContext.Consumer>
        {({setHeadRef}) => {
            return <RegisterForm {...props} setHeadRef={setHeadRef} />
        }}
    </MyContext.Consumer>
)