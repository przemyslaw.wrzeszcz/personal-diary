import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';
import supercluster from 'points-cluster';
import Marker from './Marker';
import ClusterMarker from './ClusterMarker';

import mapStyles from './mapStyles.json';

import MapWrapper from './MapWrapper';

const polskaCoords = { lat: 51.759445, lng: 19.457216 };
const MAP = {
    defaultZoom: 8,
    defaultCenter: polskaCoords,
    options: {
        styles: mapStyles,
        maxZoom: 19,
    },
};

export class GoogleMap extends React.PureComponent {
    state = {
        mapOptions: {
            center: MAP.defaultCenter,
            zoom: MAP.defaultZoom,
        },
        clusters: [],
    };

    getClusters = props => {
        const clusters = supercluster(props.data, {
            minZoom: 0,
            maxZoom: 16,
            radius: 50,
        });

        return clusters(this.state.mapOptions);
    };

    createClusters = props => {
        this.setState({
            clusters: this.state.mapOptions.bounds
                ? this.getClusters(props).map(({ wx, wy, numPoints, points }) => ({
                    lat: wy,
                    lng: wx,
                    numPoints,
                    id: `${numPoints}_${points[0].id}`,
                    points,
                }))
                : [],
        });
    };

    handleMapChange = ({ center, zoom, bounds }) => {
        this.setState(
            {
                mapOptions: {
                    center,
                    zoom,
                    bounds,
                },
            },
            () => {
                this.createClusters(this.props);
            }
        );
    };

    render() {
        return (
            <MapWrapper id="Gmap" className={`gMap ${this.props.addClass}`}>
                <GoogleMapReact
                    defaultZoom={MAP.defaultZoom}
                    defaultCenter={(this.props.center) ? this.props.center : MAP.defaultCenter}
                    options={MAP.options}
                    onChange={this.handleMapChange}
                    yesIWantToUseGoogleMapApiInternals
                >
                    {this.state.clusters.map(item => {
                        if (item.numPoints === 1) {
                            return (
                                <Marker
                                    key={item.id}
                                    lat={item.points[0].lat}
                                    lng={item.points[0].lng}
                                />
                            );
                        }

                        return (
                            <ClusterMarker
                                key={item.id}
                                lat={item.lat}
                                lng={item.lng}
                                points={item.points}
                            />
                        );
                    })}
                </GoogleMapReact>
            </MapWrapper>
        );
    }
}

export default GoogleMap;