import React from 'react';
import PropTypes from 'prop-types';

import MarkerStyled from './MarkerStyled';

class Marker extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  static defaultProps = {
      inGroup: false,
  };

  render() {
      return (
          <React.Fragment>
              {this.props.inGroup
                  ? <MarkerStyled>
                  </MarkerStyled>:<MarkerStyled>
                  </MarkerStyled>}
          </React.Fragment>
      );
  }
}

Marker.propTypes = {
  inGroup: PropTypes.bool,
};

export default Marker;
