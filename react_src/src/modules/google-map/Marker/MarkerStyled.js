import styled from 'styled-components';
import { COLORS, easyMove } from '../style-constants';

const MarkerStyled = styled.div`
  display: block;
  cursor: pointer;
  font-size: 14px;
  color: ${COLORS.green64};
  text-transform: uppercase;
  transition: transform 0.3s;
  animation: ${easyMove} 0.3s;
    border: 9px solid ${COLORS.black64};
    border-radius: 50%;
    background-color: ${COLORS.green64};
    content: "";
    width: 36px;
    height: 36px; 
    position: absolute;
    .map-customizable & {
        &:active {
            box-shadow: 0 4px 6px 0 rgba(0,0,0,0.4);
            margin-top: -26px;
            &:before {
                content: "x";
                position: absolute;
                top: 100%;
                left: 8px;
                margin-top: 10px;
                color: black;
                font-size: 16px;
                text-transform: lowercase;
            }
        }
    }
    
`;

export default MarkerStyled;
