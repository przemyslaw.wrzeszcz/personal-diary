import styled from 'styled-components';
import { easyMove } from '../style-constants';

const MarkerGroup = styled.div`
  display: flex;
  background: #fff;
  border-radius: 100px;
  animation: ${easyMove} 0.3s;
  background-color: #fff;
  width: 54px;
`;

export default MarkerGroup;
