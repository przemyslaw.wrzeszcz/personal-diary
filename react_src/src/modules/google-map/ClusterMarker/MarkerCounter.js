import styled from 'styled-components';
import { COLORS } from '../style-constants';

const MarkerCounter = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 54px;
  height: 54px;
  padding: 8px;
  margin-left: 0;
  text-align: center;
  font-size: 16px;
  border: 2px solid ${COLORS.black64};
  border-radius: 50%;
  background-color: ${COLORS.green64};
  color:  ${COLORS.white64};
`;

export default MarkerCounter;
