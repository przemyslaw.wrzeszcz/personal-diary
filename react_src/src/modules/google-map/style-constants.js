import { keyframes } from 'styled-components';

export const COLORS = {
  black64: '#000',
  white64: '#fff',
    green64: '#7bcf35'
};

export const easyMove = keyframes`
  from {
    transform: translateY(-20%);
    opacity: 0;
  }

  to {
    opacity: 1;
    transform: translateY(0);
  }
`;
