import React from 'react';
import { Player } from 'video-react';
import Poster from './img/video.jpg';
import { Container, Row, Col } from 'reactstrap';

export default class Video extends React.Component {
    render() {
        return (
            <section className="offerInfo">
                <div className="offer-description">
                    <Container>
                        <Row>
                            <Col>
                                <h3>VIDEO:</h3>
                                <Player poster={'/' + Poster}>
                                    <source src={this.props.url} />
                                </Player>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <hr/>
            </section>
        );
    }
};