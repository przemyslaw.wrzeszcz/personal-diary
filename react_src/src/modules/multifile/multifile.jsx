import React from 'react';
import FileInput from 'simple-react-file-uploader';

export default class MultiFile extends React.Component {
    render() {
        return (
            <FileInput
                multiple={true}
                onChange = {this.handleChange}
                accept="image/*"
                className="custom"
            />
        )
    }
}