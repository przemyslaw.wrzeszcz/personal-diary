import React from 'react';
import scrollToComponent from 'react-scroll-to-component';
import MyListItem from './item';
import NearOffer from './item-near';
import PaginationMod from '../pagination/pagination';
import { MyContext } from "../../context";

class ListingMod extends React.Component {
    constructor() {
        super();
        this.state = {
            currentPage: 1,
            itemsPerPage: 10,
            pageNumbers: [],
        };
        this.handlePaginationClick = this.handlePaginationClick.bind(this);
        this.myRef = React.createRef();
    }
    componentWillReceiveProps(nextProps) {
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(nextProps.data.length / this.state.itemsPerPage); i++) {
            pageNumbers.push(i);
        }

        this.setState({pageNumbers});
    }

    componentDidMount() {
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(this.props.data.length / this.state.itemsPerPage); i++) {
            pageNumbers.push(i);
        }

        this.setState({pageNumbers});
    }

    handlePaginationClick(event) {
        this.setState({
            currentPage: Number(event.target.id),
        });
        scrollToComponent(this.myRef.current, {
            align: 'top',
            duration: 1,
        });
    }

    render() {
        const { pageNumbers, currentPage, itemsPerPage } = this.state;

        const indexOfLastItem = currentPage * itemsPerPage;
        const indexOfFirstItem = indexOfLastItem - itemsPerPage;
        const currentItems = this.props.data.slice(indexOfFirstItem, indexOfLastItem);
        return (
            <section ref={this.myRef} className="listing">
                {
                    (this.props.near === true) ?
                        currentItems.map((val, id) => {
                            return (<NearOffer {...val} key={id} />)
                        })
                        :
                        currentItems.map((val, id) => {
                            return (<MyListItem {...val} key={id} myOrders={this.props.myOrders} />);
                        })
                }
                {
                    pageNumbers.length > 1 ?
                        <PaginationMod pageNumbers={pageNumbers} currentPage={currentPage} itemClick={this.handlePaginationClick} />:
                        null
                }
            </section>
        );
    }
}
export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <ListingMod {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)