import React from 'react';
import {ListGroup, ListGroupItem, Row, Col} from 'reactstrap';
import {Link} from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import TimeLeft from './time-left';
import { MyContext } from "../../context";
import settings from '../../core/settings';
import GWCommon from "../../core/common";
import locutusEmpty from "locutus/php/var/empty";

Moment.locale('pl');
momentLocalizer();

class OfferItem extends React.Component {
    render () {
        let finalClass = 'listingLink';
        if (parseInt(this.props.active) === 0) {
            finalClass+=' inactive-item';
        }
        if (parseInt(this.props.closed) === 1) {
            finalClass+=' closed-item';
        }
        return (
            <Link to={`/zamowienie/${this.props.id}/${this.props.domain_text}`} className={finalClass}>
                <ListGroup>
                    <ListGroupItem className="element">
                        <Row>
                            <Col xs="7">
                                <img src={`${settings.backendDomain}${this.props.photos_paths['120x80']}`} alt=""/>
                                <h2>{this.props.order_name}</h2>
                                <h3>{this.props.city}, {this.props.province}</h3>
                                <p className="big">{this.props.surface} ha</p>
                            </Col>
                            {
                                (!this.props.myOrders) ?
                                    <Col xs="1">
                                        {
                                            (locutusEmpty(this.props.distance) === false) ?
                                                <div className="distanceWrap">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <FontAwesome name="home"/>
                                                                    <FontAwesome name="arrows-alt-h"/>
                                                                    <FontAwesome name="map-marker"/>
                                                                    <span className="distance">{this.props.distance} km</span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                : null
                                        }
                                    </Col>:
                                    <Col xs="1">
                                        <div className="distanceWrap">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <FontAwesome name="eye"/>
                                                            <span className="distance">{this.props.visits}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <FontAwesome name="gavel"/>
                                                            <span className="distance">{this.props.offers}</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </Col>
                            }
                            <Col xs="4">
                                <ul>
                                    <li>
                                        <p className="faded">ZOSTAŁO JESZCZE:</p>
                                    </li>
                                    <li>
                                        <TimeLeft to={GWCommon.calcOfferDeadline(this.props.start_date)} />
                                    </li>
                                    <li>
                                        <p className="faded">TERMIN SKŁADANIA OFERT:</p>
                                    </li>
                                    <li>
                                        <p className="remaining">{Moment(GWCommon.calcOfferDeadline(this.props.start_date)).format('DD MMMM YYYY')}</p>
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                    </ListGroupItem>
                </ListGroup>
            </Link>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <OfferItem {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)
