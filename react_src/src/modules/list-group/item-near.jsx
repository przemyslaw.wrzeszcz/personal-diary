import React from 'react';
import {ListGroup, ListGroupItem, Row, Col} from 'reactstrap';
import {Link} from 'react-router-dom';
import TimeLeft from './time-left';
import { MyContext } from "../../context";
import settings from '../../core/settings';
import GWCommon from "../../core/common";

class Index extends React.Component {

    render () {
        return (
            <Link to={`/zamowienie/${this.props.id}/${this.props.domain_text}`} className="listingLink near">
                <ListGroup>
                    <ListGroupItem className="element">
                        <Row>
                            <Col xs="7">
                                <img src={`${settings.backendDomain}${this.props.photos_paths['90x60']}`} alt=""/>
                                <h2>{this.props.order_name}</h2>
                                <p className="big">{this.props.surface} ha</p>
                            </Col>
                            <Col xs="5">
                                <ul>
                                    <li>
                                        <p className="faded">ZOSTAŁO JESZCZE:</p>
                                    </li>
                                    <li>
                                        <TimeLeft to={GWCommon.calcOfferDeadline(this.props.start_date)}/>
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                    </ListGroupItem>
                </ListGroup>
            </Link>
        )
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <Index {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)
