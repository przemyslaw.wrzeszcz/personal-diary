import React from 'react';
import GWCommon from "../../core/common";

export default ({to}) => {
    const duration = GWCommon.calcDateDiffInDays(to);
    if (duration < 0) {
        return (
            <p className="remaining">
                {0} {GWCommon.declinationPL(0, 'dzień', 'dni', 'dni').toUpperCase()}
            </p>
        );
    }

    if (duration > 2) {
        return (
            <p className="remaining">
                {duration} {GWCommon.declinationPL(duration, 'dzień', 'dni', 'dni').toUpperCase()}
            </p>
        );
    }

    return (
        <p className="remaining">
            <span className="alert">{(duration === 0) ? 1 : duration} {GWCommon.declinationPL(duration, 'dzień', 'dni', 'dni').toUpperCase()}</span>
        </p>
    );
}
