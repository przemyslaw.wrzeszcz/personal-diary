import React from 'react';
import {Button, Form, Input, FormGroup, Label, InputGroup, Alert} from 'reactstrap';
import locutusEmpty from "locutus/php/var/empty";
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import GWCommon from "../../core/common";
import {MyContext} from "../../context";
import {validateInput} from '../validation/offerWindow';
import settings from '../../core/settings';
import axios from 'axios';
import Preloader from "../preloader/preloader";
import formPolyfillRun from 'formdata-polyfill';
import GWDatePicker from "../datepicker";
import PopupConfirm from "../popup/popup_confirm";

if (GWCommon.IsSafariBrowser()) {
    window.FormData = undefined;
    formPolyfillRun();
} else {
    formPolyfillRun();
}

Moment.locale('pl');
momentLocalizer();


class OfferWindow extends React.Component {
    constructor() {
        super();
        this.handleOnChange = this.handleOnChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.revoke = this.revoke.bind(this);
        this.state = {
            priceInputVal: "",
            priceInputGrossVal: 0,
            priceInputNettVal: 0,
            ['OrderOffersForm[start_date]']: '',
            ['OrderOffersForm[end_date]']: '',
            ['OrderOffersForm[price]']: '',
            serwerSuccessMsg: '',
            serwerErrorMsg: '',
            errors: {},
            showLoader: false,
            revokeConfirm: false,
            isClicked: false,
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleOnChange(e) {
        const priceTotal = (e.target.value * 1.08).toFixed(2);
        const priceByHectare = (e.target.value / this.props.hectareVal).toFixed(2);
        this.setState({
            priceInputVal: e.target.value,
            priceInputGrossVal: priceTotal,
            priceInputNettVal: priceByHectare,
        });
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        const clearError = this.state.errors;
        delete clearError[name];
        this.setState({
            [name]: value,
            errors: clearError,
        });
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state);

        if (!isValid) {
            this.setState({errors});
        }
        return isValid;
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            const data = new FormData(e.target);
            data.set('OrderOffersForm[end_date]', this.state['OrderOffersForm[end_date]']);
            data.set('OrderOffersForm[start_date]', this.state['OrderOffersForm[start_date]']);

            this.setState({
                showLoader: true,
            });
            const instance = axios.create({
                headers: {'Authorization': `Bearer ${this.props.userToken}`},
            });
            instance.post(`${settings.backendDomain}/orders/offers/${this.props.orderId}`, data, {headers: {'Authorization': `Bearer ${this.props.userToken}`}}).then((response) => {
                if (response.data.success) {
                    this.props.updateFn({
                        price: data.get('OrderOffersForm[price]'),
                        start_date: data.get('OrderOffersForm[start_date]'),
                        end_date: data.get('OrderOffersForm[end_date]'),
                        selected: 0,
                    });
                    this.setState({
                        serwerSuccessMsg: 'Oferta została złożona poprawnie. Jeśli zostanie wybrana przez Klienta otrzymasz maila z informacją.',
                        errors: {},
                        serwerErrorMsg: '',
                        showLoader: false,
                    });

                    return;
                }

                const newErrors = {};

                Object.entries(response.data.errors).map(errVal => {
                    newErrors[`OrderOffersForm[${errVal[0]}]`] = errVal[1][0];
                });

                this.setState({
                    serwerSuccessMsg: '',
                    errors: newErrors,
                    serwerErrorMsg: '',
                    showLoader: false,
                });
            })
                .catch((error) => {
                    this.setState({
                        showLoader: false,
                        serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.',
                    });
                });
        }
    }

    revoke() {
        const instance = axios.create({
            headers: {'Authorization': `Bearer ${this.props.userToken}`},
        });
        instance.delete(`${settings.backendDomain}/orders/offers/${this.props.orderId}`, {headers: {'Authorization': `Bearer ${this.props.userToken}`}}).then((response) => {
            if (response.data.success) {
                this.props.updateFn(false);
                this.setState({
                    serwerSuccessMsg: 'Oferta została wycofana poprawnie.',
                    errors: {},
                    serwerErrorMsg: '',
                });

                return;
            }
        })
            .catch((error) => {
                this.setState({serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.'});
            });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.myOffer) {
            const priceTotal = (nextProps.myOffer.price * 1.08).toFixed(2);
            const priceByHectare = (nextProps.myOffer.price / this.props.hectareVal).toFixed(2);
            this.setState({
                priceInputGrossVal: priceTotal,
                priceInputNettVal: priceByHectare,
            });
        }
        if (locutusEmpty(this.state['OrderOffersForm[start_date]']) && locutusEmpty(nextProps.start_date) === false) {
            this.setState({
                'OrderOffersForm[start_date]': nextProps.start_date,
            })
        }
        if (locutusEmpty(this.state['OrderOffersForm[end_date]']) && locutusEmpty(nextProps.end_date) === false) {
            this.setState({
                'OrderOffersForm[end_date]': nextProps.end_date,
            })
        }
    }

    componentDidMount() {
        if (locutusEmpty(this.state['OrderOffersForm[start_date]']) && locutusEmpty(this.props.start_date) === false) {
            this.setState({
                'OrderOffersForm[start_date]': this.props.start_date,
            })
        }
        if (locutusEmpty(this.state['OrderOffersForm[end_date]']) && locutusEmpty(this.props.end_date) === false) {
            this.setState({
                'OrderOffersForm[end_date]': this.props.end_date,
            })
        }
        if (this.props.myOffer) {
            const priceTotal = (this.props.myOffer.price * 1.08).toFixed(2);
            const priceByHectare = (this.props.myOffer.price / this.props.hectareVal).toFixed(2);
            this.setState({
                priceInputGrossVal: priceTotal,
                priceInputNettVal: priceByHectare,
            });
        }
    }

    handleClick(e) {
        e.preventDefault();
        this.setState({
            isClicked: !this.state.isClicked,
        });
    }

    render() {
        const btnClass = this.state.isClicked ? "active" : "";
        const duration = GWCommon.calcDateDiffInDays(this.props.deadline);
        const {serwerSuccessMsg, serwerErrorMsg, errors} = this.state;

        if (this.props.myOffer) {
            return (
                <React.Fragment>
                    <div className={`semi-overlay ${btnClass}`} onClick={this.handleClick}></div>
                    <aside className={`offer-window section-background ${btnClass}`}>
                        <a href="#" onClick={this.handleClick} className='btn btn-primary mobile-trigger'>OFERTA</a>
                        <FormGroup>
                            {
                                (parseInt(this.props.myOffer.selected) === 1) ? (
                                    <Alert color="success">Twoja oferta została wybrana, gratulujemy!</Alert>
                                ) : null
                            }
                            {
                                (serwerErrorMsg) ? (
                                    <Alert color="danger">{serwerErrorMsg}</Alert>
                                ) : null
                            }
                            {
                                (serwerSuccessMsg) ? (
                                    <Alert color="success">{serwerSuccessMsg}</Alert>
                                ) : null
                            }
                            <Label className="text-center">Cena całkowita za {this.props.hectareVal} h:</Label>
                            <p className="text-center">{this.props.myOffer.price}</p>
                            <div className="bottomInfo">
                                <Label className="faded align-left">{this.state.priceInputNettVal} zł za 1 ha
                                    netto</Label>
                                <Label className="faded align-right">{this.state.priceInputGrossVal} zł brutto z 8%
                                    vat</Label>
                            </div>
                        </FormGroup>
                        <hr/>
                        <p className="text-center">Proponowany termin realizacji:</p>
                        <div className="inlineForm">
                            <FormGroup>
                                <Label className="faded">TERMIN ROZPOCZĘCIA:</Label>
                                <InputGroup>
                                    <p className="text-center">{this.props.myOffer.start_date}</p>
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <Label className="faded">TERMIN ZAKOŃCZENIA:</Label>
                                <InputGroup>
                                    <p className="text-center">{this.props.myOffer.end_date}</p>
                                </InputGroup>
                            </FormGroup>
                        </div>
                        {
                            (parseInt(this.props.closedOrder) === 0 && duration >= 0) ?
                                <React.Fragment>
                                    <Button color="danger" size="lg" onClick={() => {
                                        this.setState({revokeConfirm: true});
                                    }} block>WYCOFAJ OFERTĘ</Button>
                                    <PopupConfirm
                                        isOpen={this.state.revokeConfirm}
                                        toggle={() => {
                                            this.setState({revokeConfirm: false});
                                        }}
                                        title='Potwierdź'
                                        okFN={() => {
                                            this.setState({revokeConfirm: false});
                                            this.revoke();
                                        }}
                                        okLabel='Tak, Wycofaj ofertę'
                                    >
                                        <p>Czy na pewno chcesz wycofać swoją ofertę?</p>
                                    </PopupConfirm>
                                </React.Fragment> : null
                        }
                    </aside>
                </React.Fragment>
            )
        }

        if (parseInt(this.props.closedOrder) !== 0 || duration < 0) {
            return (
                <React.Fragment>
                    <div className={`semi-overlay ${btnClass}`} onClick={this.handleClick}></div>
                    <aside className={`offer-window section-background ${btnClass}`}>
                        <a href="#" onClick={this.handleClick} className='btn btn-primary mobile-trigger'>OFERTA</a>
                        <FormGroup>
                            <Alert color="info">Niestety, upłynął już czas na składanie ofert do tego zlecenia.</Alert>
                        </FormGroup>
                    </aside>
                </React.Fragment>
            )
        }

        return (
            <React.Fragment>
                <div className={`semi-overlay ${btnClass}`} onClick={this.handleClick}></div>
                <aside className={`offer-window section-background ${btnClass}`}>
                    <a href="#" onClick={this.handleClick} className='btn btn-primary mobile-trigger'>OFERTA</a>
                    {
                        (this.state.showLoader) ? (
                            <Preloader/>
                        ) : null
                    }
                    <Form onSubmit={this.onSubmit}>
                        <FormGroup>
                            {
                                (serwerErrorMsg) ? (
                                    <Alert color="danger">{serwerErrorMsg}</Alert>
                                ) : null
                            }
                            {
                                (serwerSuccessMsg) ? (
                                    <Alert color="success">{serwerSuccessMsg}</Alert>
                                ) : null
                            }
                            <Label className="text-center">Cena całkowita za {this.props.hectareVal} h:</Label>
                            <Input className="textNumber" type="number" name="OrderOffersForm[price]" id="price"
                                   value={this.state.priceInputVal} onChange={this.handleOnChange}/>
                            <span className="error">{errors['OrderOffersForm[price]']}</span>
                            <div className="bottomInfo">
                                <Label className="faded align-left">{this.state.priceInputNettVal} zł za 1 ha
                                    netto</Label>
                                <Label className="faded align-right">{this.state.priceInputGrossVal} zł brutto z 8%
                                    vat</Label>
                            </div>
                        </FormGroup>
                        <hr/>
                        <p className="text-center">Proponowany termin realizacji:</p>
                        <div className="inlineForm">
                            <FormGroup>
                                <Label className="faded">TERMIN ROZPOCZĘCIA:</Label>
                                <InputGroup>
                                    <GWDatePicker
                                        value={this.state['OrderOffersForm[start_date]']}
                                        placeholder="Kliknij by wybrać datę"
                                        onChange={value => {
                                            const dateEnd = this.state.errors;
                                            delete dateEnd['OrderOffersForm[start_date]'];
                                            this.setState({
                                                ['OrderOffersForm[start_date]']: value,
                                                errors: dateEnd,
                                            });
                                        }}
                                    />
                                    <span className="error">{errors['OrderOffersForm[start_date]']}</span>
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <Label className="faded">TERMIN ZAKOŃCZENIA:</Label>
                                <InputGroup>
                                    <GWDatePicker
                                        value={this.state['OrderOffersForm[end_date]']}
                                        placeholder="Kliknij by wybrać datę"
                                        onChange={value => {
                                            const dateEnd = this.state.errors;
                                            delete dateEnd['OrderOffersForm[end_date]'];
                                            this.setState({
                                                ['OrderOffersForm[end_date]']: value,
                                                errors: dateEnd,
                                            });
                                        }}
                                    />
                                    <span className="error">{errors['OrderOffersForm[end_date]']}</span>
                                </InputGroup>
                            </FormGroup>
                        </div>
                        <Button color="primary" size="lg" block>ZŁÓŻ OFERTĘ</Button>
                    </Form>
                </aside>
            </React.Fragment>
        );
    }
}


export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <OfferWindow {...props} userData={state.user} userToken={state.token}/>
        }}
    </MyContext.Consumer>
)
