import React, {Component} from 'react';
import Listing from '../list-group/listing';
import { MyContext } from "../../context";
import axios from 'axios';
import settings from '../../core/settings';
import {Link} from 'react-router-dom';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listingData: [],
            renderError: false,
        };
    }

    componentDidMount() {
        if (this.props.userToken.length > 0) {
            if (this.props.userData.role !== 'executor') {
                const getQuestionsUrl = `${settings.backendDomain}/orders/client/listing`;
                const instance = axios.create({
                    headers: {'Authorization': `Bearer ${this.props.userToken}`},
                });
                instance.get(getQuestionsUrl, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                    .then(response => {
                        this.setState({
                            listingData: response.data.listing,
                        });
                    }).catch((error) => {
                        this.setState({
                            renderError: true,
                        });
                    });
            }
        }
    }

    render() {

        return (
            <React.Fragment>
                {(this.state.listingData.length > 0) ?
                    <Listing data={this.state.listingData} myOrders />:
                    <h5>Nie dodałeś jeszcze żadnego zlecenia.<br/> Możesz dodać je <Link to="/moje-konto/dodaj-zamowienie">tutaj</Link></h5>
                }

            </React.Fragment>
        );
    }
}


export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <Index {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)

