import React from 'react';
import Cropper from 'react-cropper';
import {Button} from 'reactstrap';
import GWCommon from "../../core/common";
import Cropup from '../popup/cropperPopup';
import 'cropperjs/dist/cropper.css';
import FileInput from 'simple-react-file-uploader';
import axios from 'axios';
import { MyContext } from "../../context";
import Preloader from '../preloader/preloader';
import settings from '../../core/settings';
import formPolyfillRun from 'formdata-polyfill';

if (GWCommon.IsSafariBrowser()) {
    window.FormData = undefined;
    formPolyfillRun();
} else {
    formPolyfillRun();
}

class ImageCropperModule extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onCrop = this.onCrop.bind(this);
        this.toggleCropPopup = this.toggleCropPopup.bind(this);
        this.cropPopupReset = this.cropPopupReset.bind(this);
        this.savePhoto = this.savePhoto.bind(this);
        this.state = {
            cropPopup: false,
            imageLoaded: false,
            photoUpload: "",
            isLoading: false,
            dataToSave: {
                name: "",
                code: "1920x1080",
                x: 0,
                y: 0,
                width: 0,
                height: 0,
            },
        }
    }

    toggleCropPopup() {
        this.setState({
            cropPopup: !this.state.cropPopup,
        });
    }

    cropPopupReset() {
        this.setState({
            cropPopup: false,
            imageLoaded: false,
            photoUpload: "",
            isLoading: false,
            dataToSave: {
                name: "",
                code: "1920x1080",
                x: 0,
                y: 0,
                width: 0,
                height: 0,
            },
        });
    }

    onCrop(data) {
        const dataToSave = this.state.dataToSave;
        dataToSave.x = data.detail.x;
        dataToSave.y = data.detail.y;
        dataToSave.width = data.detail.width;
        dataToSave.height = data.detail.height;

        this.setState({
            dataToSave,
        });
    }

    savePhoto() {
        this.setState({
            isLoading: true,
        });

        const instance = axios.create({
            headers: {'Authorization': `Bearer ${this.props.userToken}`},
        });

        instance.post(`${settings.backendDomain}/photos`, this.state.dataToSave, {headers: {'Authorization': `Bearer ${this.props.userToken}`}}).then((response) => {
            if (response.data.success) {
                this.props.pushPhoto({
                    display: response.data.image,
                    fileName: this.state.dataToSave.name,
                });
                this.cropPopupReset();

                return;
            }

            this.setState({
                isLoading: false,
            });
        }).catch((error) => {
            this.setState({
                isLoading: false,
            });
        });
    }

    onChange(arr) {
        if (arr.length > 0) {
            this.setState({
                isLoading: true,
            });

            const instance = axios.create({
                headers: {'Authorization': `Bearer ${this.props.userToken}`},
            });
            const data = new FormData();
            data.set('PhotoForm[photo]', arr[0], arr[0].name);
            instance.post(`${settings.backendDomain}/photos/upload`, data, {headers: {'Authorization': `Bearer ${this.props.userToken}`}}).then((response) => {
                if (response.data.success) {
                    const dataToSave = this.state.dataToSave;
                    dataToSave.name = response.data.name;
                    this.setState({
                        isLoading: false,
                        imageLoaded: true,
                        photoUpload: response.data.urld,
                        dataToSave: dataToSave,
                    });

                    return;
                }

                this.setState({
                    isLoading: false,
                });
            }).catch((error) => {
                this.setState({
                    isLoading: false,
                });
            });

            return;
        }

        this.setState({
            imageLoaded: false,
        })
    }

    render() {
        const { isLoading, imageLoaded, photoUpload, cropPopup } = this.state;
        const calcRatio = GWCommon.getAspectRatio(16, 9);
        return (
            <React.Fragment>
                {
                    isLoading ?
                        <Preloader />
                        :<React.Fragment>
                            <Button onClick={this.toggleCropPopup}>Dodaj zdjęcie</Button>
                            <Cropup showSave={imageLoaded} saveFN={this.savePhoto} toggle={this.cropPopupReset} isOpen={cropPopup} title="Przytnij zdjęcie">
                                {
                                    imageLoaded ?
                                        <div className="gw-crop-cnt"><Cropper
                                            src={`${settings.backendDomain}/photos?file=${photoUpload}`}
                                            guides={false}
                                            crop={this.onCrop}
                                            viewMode={3}
                                            aspectRatio={calcRatio.w / calcRatio.h}/></div>
                                        : <FileInput
                                            multiple={false}
                                            onChange={this.onChange}
                                            accept="image/*"
                                            className="custom"
                                        />
                                }
                            </Cropup>
                        </React.Fragment>
                }
            </React.Fragment>
        )
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <ImageCropperModule {...props} userToken={state.token} />
        }}
    </MyContext.Consumer>
)