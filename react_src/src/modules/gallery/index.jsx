import React from 'react';
import Slider from "react-slick";
import {Container, Row, Col} from 'reactstrap';
import Lightbox from 'react-image-lightbox';
import FontAwesome from 'react-fontawesome';
import 'react-image-lightbox/style.css';
import settings from "../../core/settings";

function CustomNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className="slick-arrow arrow-right"
            onClick={onClick}>
            <FontAwesome name="arrow-circle-right"/>
        </div>
    );
}

function CustomPrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className="slick-arrow arrow-left"
            onClick={onClick}>
            <FontAwesome name="arrow-circle-left"/>
        </div>
    );
}

class GalleryPhotos extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return(
            <div>
                <span className="slide">
                    <img src={this.props.src} onClick={this.props.onClick} alt={this.props.alt} />
                </span>
            </div>
        )
    }
}
export default class GallerySection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current: '',
            isOpen: false,
        };
        /*this.getSliderSettings = this.getSliderSettings.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);*/
    }
    getSliderSettings(){
        return{
            dots: false,
            arrows: true,
            infinite: false,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            nextArrow: <CustomNextArrow />,
            prevArrow: <CustomPrevArrow />,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    },
                },
                {
                    breakpoint: 680,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
            ],
        }
    };
    handleCloseModal = (e) => {
        this.setState({
            current: '',
            isOpen: false,
        })
    };

    render() {
        const settingsgallery = this.getSliderSettings();
        const { isOpen, current } = this.state;

        return (
            <section className="offerInfo">
                <div className="offer-description">
                    <Container>
                        <Row>
                            <Col>
                                <h3>GALERIA:</h3>
                                <Slider {...settingsgallery}>
                                    {
                                        this.props.photos.map((val, id) => {
                                            return (
                                                <GalleryPhotos onClick={() => {this.setState({ isOpen: true, current: id });}} key={id} alt="mulczer" src={`${settings.backendDomain}${val["278x208"]}`} />
                                            )
                                        })
                                    }
                                    {isOpen && (
                                        <Lightbox
                                            mainSrc={`${settings.backendDomain}${this.props.photos[current]['1920x1080']}`}
                                            onCloseRequest={this.handleCloseModal}
                                            nextSrc={settings.backendDomain + this.props.photos[(current + 1) % this.props.photos.length]['1920x1080']}
                                            prevSrc={settings.backendDomain + this.props.photos[(current + this.props.photos.length - 1) % this.props.photos.length]['1920x1080']}
                                            onMovePrevRequest={() =>
                                                this.setState({
                                                    current: (current + this.props.photos.length - 1) % this.props.photos.length,
                                                })}
                                            onMoveNextRequest={() =>
                                                this.setState({
                                                    current: (current + 1) % this.props.photos.length,
                                                })}
                                        />
                                    )}
                                </Slider>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </section>
        );
    }
}