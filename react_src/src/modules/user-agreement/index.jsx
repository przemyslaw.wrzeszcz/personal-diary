import React, {Component} from 'react';
import {Form, FormGroup, Row, Col, Label, Input, Button, Alert, CustomInput} from 'reactstrap';
import {validateInput}  from '../validation/userInvoice';
import axios from 'axios';
import { MyContext } from "../../context";
import settings from '../../core/settings';
import provinces from "../../core/provinces";
import PlacesAutocomplete from 'react-places-autocomplete';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nip: (props.userData.nip) ? props.userData.nip : '',
            company_name: (props.userData.company_name) ? props.userData.company_name : '',
            company_province: (props.userData.company_province) ? props.userData.company_province : '',
            company_city: (props.userData.company_city) ? props.userData.company_city : '',
            company_address: (props.userData.company_address) ? props.userData.company_address : '',
            errors: {},
            isLoading: false,
            serwerErrorMsg: '',
            serwerSuccessMsg: '',

        };
        this.onChange = this.onChange.bind(this);
        this.handleAutocompleteChange = this.handleAutocompleteChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleAutocompleteChange(address) {
        const arr = address.split(",");
        this.setState({
            company_city: arr[0].trim(),
        });
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }
        return isValid;
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            const registerApiUrl = `${settings.backendDomain}/users/invoice`;
            const payload={
                "company_name": this.state.company_name,
                "company_province": this.state.company_province,
                "company_city": this.state.company_city,
                "company_address": this.state.company_address,
                "nip": this.state.nip,
            };

            const instance = axios.create({
                headers: {'Authorization': `Bearer ${this.props.userToken}`},
            });

            instance.put(registerApiUrl, payload, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                .then((response) => {
                    if (response.data.success) {
                        this.setState({
                            serwerSuccessMsg: 'Zmiany wprowadzone pomyślnie!',
                            errors: {},
                            serwerErrorMsg: '',
                        });
                        const newUser = this.props.userData;
                        newUser.company_name = payload.company_name;
                        newUser.company_province = payload.company_province;
                        newUser.company_city = payload.company_city;
                        newUser.company_address = payload.company_address;
                        newUser.nip = payload.nip;
                        this.props.setUser(newUser);
                        return;
                    }

                    const errorMaping = {
                        "company_name": 'company_name',
                        "company_province": 'company_province',
                        "company_city": 'company_city',
                        "company_address": 'company_address',
                        "nip": 'nip',
                    };
                    const errors = {};
                    Object.keys(response.data.errors).forEach((key) => {
                        errors[errorMaping[key]] = response.data.errors[key];
                    });
                    this.setState({ errors });
                })
                .catch((error) => {
                    this.setState({ serwerErrorMsg: 'Wystąpił nieoczekiwany błąd. Prosimy spróbować ponownie.' });
                });
        }
    }

    render() {
        const {nip, company_name, company_province, company_city, company_address, errors, isLoading, serwerErrorMsg, serwerSuccessMsg} = this.state;
        return (
            <section className="userData">
                <Form onSubmit={this.onSubmit}>
                    <Row>
                        <Col xs="12">
                            <h5>Dane do faktury</h5>

                            {
                                (serwerErrorMsg) ? (
                                    <Alert color="danger">{serwerErrorMsg}</Alert>
                                ) : null
                            }
                            {
                                (serwerSuccessMsg) ? (
                                    <Alert color="success">{serwerSuccessMsg}</Alert>
                                ) : null
                            }

                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <FormGroup row>
                                <Label for="userCompanyName" sm="3">Nazwa firmy</Label>
                                <Col sm="9">
                                    <Input value={company_name} type="text" name="company_name" id="userCompanyName" onChange={this.onChange} />
                                    <span className="error">{errors.company_name}</span>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="userNip" sm="3">NIP</Label>
                                <Col sm="9">
                                    <Input value={nip} type="number" name="nip" id="userNip" onChange={this.onChange} />
                                    <span className="error">{errors.nip}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label for="userCompanyProvince" sm="3">Województwo firmy</Label>
                                <Col sm="9">
                                    <CustomInput value={company_province} type="select" id="userCompanyProvince" name="company_province" onChange={this.onChange}>
                                        <option value="">Wybierz</option>
                                        {
                                            provinces.map((val) => (
                                                <option key={val} value={val}>{val}</option>
                                            ))
                                        }
                                    </CustomInput>
                                    <span className="error">{errors.company_province}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label for="userCompanyCity" sm="3">Miasto firmy</Label>
                                <Col sm="9">
                                    <PlacesAutocomplete
                                        value={company_city}
                                        onChange={this.handleAutocompleteChange}
                                        searchOptions={
                                            {
                                                types: ['(cities)'],
                                                componentRestrictions: {country: 'pl'},
                                            }
                                        }
                                    >
                                        {({getInputProps, suggestions, getSuggestionItemProps, loading}) => (
                                            <div className="autocomplete-wrapper">
                                                <Input
                                                    {...getInputProps({
                                                        placeholder: 'Wybierz miasto..',
                                                        className: 'location-search-input',
                                                    })}
                                                />
                                                <div className="autocomplete-dropdown-container">
                                                    {loading && <div>Loading...</div>}
                                                    {suggestions.map(suggestion => {
                                                        const className = suggestion.active
                                                            ? 'suggestion-item--active'
                                                            : 'suggestion-item';
                                                        // inline style for demonstration purpose
                                                        const style = suggestion.active
                                                            ? {backgroundColor: '#fafafa', cursor: 'pointer'}
                                                            : {backgroundColor: '#ffffff', cursor: 'pointer'};
                                                        return (
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className,
                                                                    style,
                                                                })}
                                                            >
                                                                <span>{suggestion.formattedSuggestion.mainText}</span>
                                                            </div>
                                                        );
                                                    })}
                                                </div>
                                            </div>
                                        )}
                                    </PlacesAutocomplete>
                                    <span className="error">{errors.company_city}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="12">
                            <FormGroup row>
                                <Label for="userCompanyAddress" sm="3">Adres firmy</Label>
                                <Col sm="9">
                                    <Input value={company_address} type="text" name="company_address" id="userCompanyAddress" onChange={this.onChange} />
                                    <span className="error">{errors.company_address}</span>
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <div className="text-center">
                                <Button color="primary" type="submit">Zapisz</Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </section>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({ setUser, state }) => {
            return <Index {...props} setUser={setUser} userData={state.user} userToken={state.token} />
        }}
    </MyContext.Consumer>
)