import Leaflet from 'leaflet';
Leaflet.Icon.Default.imagePath =
    '//cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/images/';
import React from 'react';
import {Link} from 'react-router-dom';
import TimeLeft from '../list-group/time-left';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import 'leaflet/dist/leaflet.css';
import 'react-leaflet-markercluster/dist/styles.min.css';

const polskaCoords = { lat: 51.759445, lng: 19.457216 };
const MAP = {
    defaultZoom: 6,
    defaultCenter: polskaCoords,
    options: {
        maxZoom: 19,
    },
};

export default class LeafletMap extends React.Component {
    state = {
        mapOptions: {
            center: MAP.defaultCenter,
            zoom: MAP.defaultZoom,
        },
        clusters: [],
    };

    render() {
        // console.log(this.props.data); return null;
        return (
            <Map center={(this.props.center) ? this.props.center : MAP.defaultCenter} zoom={MAP.defaultZoom} maxZoom={MAP.options.maxZoom} scrollWheelZoom={false} className={this.props.addClass}>
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                />
                {
                    (this.props.data.length <= 0) ? null :
                        (this.props.data.length > 1) ?
                            <MarkerClusterGroup>
                                {
                                    this.props.data.map((val, id) =>
                                        <Marker key={id} position={{lat: val.lat, lng: val.lng}}>
                                            {
                                                (!this.props.withoutPopup) ?
                                                    <Popup>
                                                        <p><strong>{val.order_name}</strong></p>
                                                        <p>{val.city}, {val.province}</p>
                                                        <p>ZOSTAŁO JESZCZE:</p>
                                                        <TimeLeft to={val.start_date} />
                                                        <Link to={`/zamowienie/${val.id}/${val.domain_text}`} className="listingLink">Przejdź do zlecenia</Link>
                                                    </Popup> : null
                                            }
                                        </Marker>
                                    )
                                }
                            </MarkerClusterGroup> : <Marker position={{lat: this.props.data[0].lat, lng: this.props.data[0].lng}}>
                                {
                                    (!this.props.withoutPopup) ?
                                        <Popup>
                                            <p>{this.props.data[0].order_name}</p>
                                            <p><strong>{this.props.data[0].city}, {this.props.data[0].province}</strong></p>
                                            <ul>
                                                <li>
                                                    <p className="faded">ZOSTAŁO JESZCZE: <TimeLeft to={this.props.data[0].start_date} /></p>
                                                </li>
                                            </ul>
                                            <Link to={`/zamowienie/${this.props.data[0].id}/${this.props.data[0].domain_text}`} className="listingLink">Przejdź do zlecenia</Link>
                                        </Popup>:null
                                }
                            </Marker>
                }
            </Map>
        );
    }
}