import React from "react";
import { Collapse, CardBody, Card } from 'reactstrap';

export default class SingleSearchFilterGroup extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            collapse: true,
        };
    }

    toggle(e) {
        e.stopPropagation();
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
        e.nativeEvent.preventDefault();
        this.setState({ collapse: !this.state.collapse });
    }

    render() {
        return (
            <div className="element">
                <a href="#" className="SearchToggle" onClick={this.toggle}>{this.props.title}:</a>
                <Collapse isOpen={this.state.collapse}>
                    <Card>
                        <CardBody className={this.props.cardBodyClass}>
                            { this.props.children }
                        </CardBody>
                    </Card>
                </Collapse>
            </div>
        );
    }
}
