import React from 'react';
import { FormGroup, Form, Label, Input, Button, InputGroup, InputGroupAddon } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import qs from 'qs';
import { MyContext } from "../../context";
import SingleSearchFilterGroup from "./single-search-filter-group";
import provinces from '../../core/provinces';
import scrollToComponent from "react-scroll-to-component";
import { DateTimePicker } from 'react-widgets';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import axios from "axios";
import settings from "../../core/settings";
import locutusEmpty from "locutus/php/var/empty";
import formPolyfillRun from 'formdata-polyfill';
import GWCommon from "../../core/common";

if (GWCommon.IsSafariBrowser()) {
    window.FormData = undefined;
    formPolyfillRun();
} else {
    formPolyfillRun();
}

Moment.locale('pl');
momentLocalizer();

class SearchSection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            provinces: {},
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.clearSearch = this.clearSearch.bind(this);
        this.fetchProvinces = this.fetchProvinces.bind(this);
        this.myFormRef = null;
    }

    fetchProvinces() {
        const instance = axios.create({
            headers: {'Authorization': `Bearer ${this.props.userToken}`},
        });
        this.serverRequest =
            instance
                .get(`${settings.backendDomain}/orders/provinces`, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                .then((result) => {
                    const tempState = {};
                    provinces.map((val) => {
                        if (result.data.hasOwnProperty(val)) {
                            tempState[val] = (result.data[val] !== undefined) ? result.data[val] : 0;
                        } else {
                            tempState[val] = 0;
                        }
                    });
                    this.setState({
                        provinces: tempState,
                    });
                }).catch((e) => {
                    const tempState = {};
                    provinces.map((val) => {
                        tempState[val] = '?';
                    });
                    this.setState({
                        provinces: tempState,
                    });
                });
    }

    componentDidMount() {
        if (this.props.userToken.length > 0) {
            if (locutusEmpty(this.props.userData.role) === false && this.props.userData.role !== 'customer') {
                this.fetchProvinces();
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userToken.length > 0) {
            if (locutusEmpty(this.props.userData.role) !== false && (locutusEmpty(nextProps.userData.role) === false && nextProps.userData.role !== 'customer')) {
                this.fetchProvinces();
            }
        }
    }

    componentWillUnmount() {
        if (this.serverRequest !== undefined && typeof (this.serverRequest.abort) === 'function') {
            this.serverRequest.abort();
        }
    }

    onSubmit(e) {
        e.stopPropagation();
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
        e.nativeEvent.preventDefault();

        const data = new FormData(e.target);
        let final = '';
        data.forEach(function(value, key){
            if (final.length > 0) {
                final += '&';
            }

            const tmpObj = {};
            tmpObj[key] = value;

            final += qs.stringify(tmpObj);
        });

        scrollToComponent(this.props.headerRef, {
            align: 'top',
            duration: 1,
        });

        this.props.updateData(final);
    }

    clearSearch(e) {
        e.stopPropagation();
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
        e.nativeEvent.preventDefault();

        scrollToComponent(this.props.headerRef, {
            align: 'top',
            duration: 1,
        });

        if (this.myFormRef !== null) {
            this.myFormRef.reset();
        }

        this.props.updateData('');
    }

    render() {
        return (
            <aside className="search-section section-background">
                <Form innerRef={(el) => {this.myFormRef = el;}} onSubmit={this.onSubmit}>
                    <SingleSearchFilterGroup title="BRANŻA">
                        <React.Fragment>
                            {
                                this.props.industries.map((val, id) => (
                                    <FormGroup key={id} check>
                                        <Label check>
                                            <Input name="search[industries][]" value={val.id} type="checkbox"/>{' '}
                                            {val.name}
                                        </Label>
                                    </FormGroup>
                                ))
                            }
                        </React.Fragment>
                    </SingleSearchFilterGroup>
                    <SingleSearchFilterGroup title="WOJEWÓDZTWO">
                        {
                            provinces.map((val) => (
                                <FormGroup key={val} check>
                                    <Label check>
                                        <Input name="search[province][]" value={val} type="checkbox" />{` ${val} (${this.state.provinces[val]})`}
                                    </Label>
                                </FormGroup>
                            ))
                        }
                    </SingleSearchFilterGroup>
                    <SingleSearchFilterGroup title="POWIERZCHNIA" cardBodyClass="SmallerInputs">
                        <div className="form-inline">
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Label for="lowNumber" className="mr-sm-2 faded">Od</Label>
                                <Input type="number" name="search[surface_from]" id="lowNumber" />
                                <Label for="lowNumber" className="mr-sm-2">H</Label>
                            </FormGroup>
                            <span className="separator">-</span>
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Label for="highNumber" className="mr-sm-2 faded">Do</Label>
                                <Input type="number" name="search[surface_to]" id="highNumber" />
                                <Label for="highNumber" className="mr-sm-2">H</Label>
                            </FormGroup>
                        </div>
                    </SingleSearchFilterGroup>
                    <SingleSearchFilterGroup title="TERMIN ROZPOCZĘCIA PRAC" cardBodyClass="SmallerInputs">
                        <div className="form-inline">
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Label for="lowStartDate" className="mr-sm-2 faded">Od</Label>
                                <DateTimePicker time={false} format="YYYY-MM-DD" name="search[start_date_from]" id="lowStartDate" />
                            </FormGroup>
                            <span className="separator">-</span>
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Label for="highStartDate" className="mr-sm-2 faded">Do</Label>
                                <DateTimePicker time={false} format="YYYY-MM-DD" name="search[start_date_to]" id="highStartDate" />
                            </FormGroup>
                        </div>
                    </SingleSearchFilterGroup>
                    <SingleSearchFilterGroup title="TERMIN ZAKOŃCZENIA PRAC" cardBodyClass="SmallerInputs">
                        <div className="form-inline">
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Label for="lowDate" className="mr-sm-2 faded">Od</Label>
                                <DateTimePicker time={false} format="YYYY-MM-DD" name="search[end_date_from]" id="lowDate" />
                            </FormGroup>
                            <span className="separator">-</span>
                            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                <Label for="highDate" className="mr-sm-2 faded">Do</Label>
                                <DateTimePicker time={false} format="YYYY-MM-DD" name="search[end_date_to]" id="highDate" />
                            </FormGroup>
                        </div>
                    </SingleSearchFilterGroup>
                    <SingleSearchFilterGroup title="STOPIEŃ TRUDNOŚCI">
                        <FormGroup check>
                            <Label check>
                                <Input name="search[difficulty_level][]" value="1" type="checkbox" />{' '}
                                Bardzo łatwy
                            </Label>
                        </FormGroup>
                        <FormGroup check>
                            <Label check>
                                <Input name="search[difficulty_level][]" value="2" type="checkbox" />{' '}
                                Łatwy
                            </Label>
                        </FormGroup>
                        <FormGroup check>
                            <Label check>
                                <Input name="search[difficulty_level][]" value="3" type="checkbox" />{' '}
                                Średni
                            </Label>
                        </FormGroup>
                        <FormGroup check>
                            <Label check>
                                <Input name="search[difficulty_level][]" value="4" type="checkbox" />{' '}
                                Trudny
                            </Label>
                        </FormGroup>
                        <FormGroup check>
                            <Label check>
                                <Input name="search[difficulty_level][]" value="5" type="checkbox" />{' '}
                                Bardzo trudny
                            </Label>
                        </FormGroup>
                    </SingleSearchFilterGroup>
                    <InputGroup className="searchPaddingBot">
                        <Input name="search[description]" placeholder="wyszukaj w opisie" />
                        <InputGroupAddon addonType="append"><Button color="primary"><FontAwesome name="search"/></Button></InputGroupAddon>
                    </InputGroup>
                    <div className={(this.props.currentSearch.length > 0) ? "searchPaddingBot" : ''}>
                        <Button color="primary" size="lg" block>FILTRUJ</Button>
                    </div>
                    {
                        (this.props.currentSearch.length > 0) ?
                            <Button color="secondary" size="lg" onClick={this.clearSearch} block>WYCZYŚĆ FILTR</Button> : null
                    }
                </Form>
            </aside>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <SearchSection {...props} headerRef={state.headerRef} industries={state.industries} userData={state.user} userToken={state.token} />
        }}
    </MyContext.Consumer>
)
