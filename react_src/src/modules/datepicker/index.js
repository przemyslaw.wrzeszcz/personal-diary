import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import locutusEmpty from "locutus/php/var/empty";
import FontAwesome from 'react-fontawesome';

import "react-datepicker/dist/react-datepicker.css";

export default class GWDatePicker extends React.Component {
    constructor() {
        super();
        this.state = {
            startDate: null,
            isOpen: false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.toggleCalendar = this.toggleCalendar.bind(this);
    }

    handleChange(date) {
        this.setState({startDate: date});

        if (typeof (this.props.onChange) === 'function') {
            const valToSet = date.format("YYYY-MM-DD");
            this.props.onChange(valToSet)
        }

        this.toggleCalendar()
    }

    toggleCalendar(e) {
        if (locutusEmpty(e) === false) {
            e.stopPropagation();
            e.preventDefault();
            if (locutusEmpty(e.nativeEvent) === false) {
                e.nativeEvent.stopImmediatePropagation();
                e.nativeEvent.preventDefault();
            }
        }
        this.setState({isOpen: !this.state.isOpen});
    }

    componentDidMount() {
        if (locutusEmpty(this.props.value) === false) {
            this.setState({
                startDate: moment(this.props.value),
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (locutusEmpty(nextProps.value) === false && this.props.value !== nextProps.value) {
            this.setState({
                startDate: moment(nextProps.value),
            });
        }
    }

    render() {
        return (
            <React.Fragment>
                <a href="#"
                    className={`gw-date-input ${this.props.className}`}
                    onClick={this.toggleCalendar}>
                    { (locutusEmpty(this.state.startDate) ? this.props.placeholder : this.state.startDate.format("YYYY-MM-DD")) }
                    <span className='button-dummy'>
                        <FontAwesome name='calendar-alt'/>
                    </span>
                </a>
                {
                    this.state.isOpen && (
                        <DatePicker
                            selected={this.state.startDate}
                            onChange={this.handleChange}
                            locale="pl"
                            withPortal
                            inline
                            onClickOutside={this.toggleCalendar}
                        />
                    )
                }
            </React.Fragment>
        )
    }
}
