import React, {Component} from 'react';
import Listing from '../list-group/listing';
import {Container, Row, Col} from 'reactstrap';
import { MyContext } from "../../context";
import settings from '../../core/settings';
import axios from 'axios';

class Index extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <section className="offerInfo">
                    <div className="offer-description">
                        <Container fluid>
                            <Row>
                                <Col>
                                    <h3>W POBLIŻU:</h3>
                                    <Listing near data={this.props.data} />
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <Index {...props} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)
