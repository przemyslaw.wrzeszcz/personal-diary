import React from 'react';
import { Form, FormGroup, Label, Input, Button, Alert } from 'reactstrap';
import {Link, Redirect} from 'react-router-dom';
import {validateInput}  from '../validation/login';
import axios from 'axios';
import { MyContext } from "../../context";
import settings from '../../core/settings';
import Preloader from "../preloader/preloader";
import locutusEmpty from "locutus/php/var/empty";


class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Iemail: '',
            password: '',
            errors: {},
            serwerErrorMsg: '',
            isLoading: false,
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    isValid() {
        const {errors, isValid} = validateInput(this.state);

        if (!isValid) {
            this.setState({ errors });
        }
        return isValid;
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.isValid()) {
            this.setState({isLoading: true});
            const loginApiUrl = `${settings.backendDomain}/users`;
            const userDataApiUrl = `${settings.backendDomain}/users/basic`;
            const payload={
                "mail": this.state.Iemail,
                "password": this.state.password,
            };
            axios.post(loginApiUrl, payload)
                .then((response) => {
                    if (response.data.success) {
                        // zalogowało nas, token siedzi tu: response.data.token
                        // tu pobierz szczegóły
                        axios.get(userDataApiUrl, {headers: {'Authorization': `Bearer ${response.data.token}`}})
                            .then((detailsResponse)=> {
                                this.props.setToken(response.data.token);
                                this.props.setUser(detailsResponse.data.user);
                            })
                            .catch(() =>{
                                this.setState({
                                    serwerErrorMsg: 'wystąpił nieoczekiwany błąd prosimy spróbować ponownie',
                                    isLoading: false,
                                });
                                setTimeout(() => {
                                    this.setState({serwerErrorMsg: ''});
                                }, 3000)
                            });
                        return;
                    }
                    // Jeśli tutaj jesteśmy to serwer zwrócił błąd, trzeba coś z nim zrobić
                    this.setState({
                        serwerErrorMsg: response.data.msg,
                        isLoading: false,
                    });
                    setTimeout(() => {
                        this.setState({serwerErrorMsg: ''});
                    }, 3000)
                })
                .catch((error) => {
                    this.setState({
                        serwerErrorMsg: 'wystąpił nieoczekiwany błąd prosimy spróbować ponownie',
                        isLoading: false,
                    });
                    setTimeout(() => {
                        this.setState({serwerErrorMsg: ''});
                    }, 3000)
                    // jak tutaj wpadamy to pokaż userowi komunikat że wystąpił nieoczekiwany błąd i niech spróbuje ponownie
                });
        }
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        const { errors, Iemail, password, serwerErrorMsg, isLoading } = this.state;
        if (this.props.userToken.length > 0) {
            // sessionStorage.setItem('abvReactCache', );
            if (this.props.userData.role === 'customer') {
                return <Redirect to="/moje-konto"/>
            }

            return <Redirect to="/"/>
        }
        return (
            <section className="loginForm">
                {
                    (isLoading) ? (
                        <Preloader />
                    ) : null
                }
                <Form onSubmit={this.onSubmit}>
                    <h1 className="text-center">Logowanie</h1>
                    <Alert isOpen={(!locutusEmpty(serwerErrorMsg))} color="danger">{serwerErrorMsg}</Alert>
                    <FormGroup>
                        <Label for="loginEmail">Email</Label>
                        <Input type="email" name="Iemail" id="loginEmail" placeholder="Email" value={Iemail} error={errors.Iemail} onChange={this.onChange} />
                        <span className="error">{errors.Iemail}</span>
                    </FormGroup>
                    <FormGroup>
                        <Label for="loginPassword">Hasło</Label>
                        <Input type="password" name="password" id="loginPassword" placeholder="Hasło" value={password} error={errors.password} onChange={this.onChange} />
                        <span className="error">{errors.password}</span>
                    </FormGroup>
                    {/*<FormGroup check>
                        <Label check>
                            <Input type="checkbox" />{' '}
                            Zapamiętaj mnie
                        </Label>
                    </FormGroup>*/}
                    <Button color="primary" size="lg" block type="submit">Zaloguj się</Button>
                    <br/>
                    <Label>Nie posiadasz konta?</Label>
                    <Link exact="true" to="/rejestracja" className="btn btn-secondary btn-lg btn-block">Zarejestruj się</Link>
                </Form>
            </section>
        )
    }
}

export default props => (
    <MyContext.Consumer>
        {({setToken, setUser, state}) => {
            return <LoginForm {...props} setToken={setToken} setUser={setUser} userToken={state.token} userData={state.user} />
        }}
    </MyContext.Consumer>
)
