import React from 'react';
import {Container, Row, Col, ListGroup, ListGroupItem} from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import 'react-image-lightbox/style.css';
import settings from "../../core/settings";

export default ({order_id, files}) => {
    return (
        <section className="offerInfo">
            <div className="offer-description">
                <Container>
                    <Row>
                        <Col>
                            <h3>Dodatkowe pliki:</h3>
                            <ListGroup flush className="order-file-list">
                                {
                                    files.map(
                                        (val, id) => (
                                            <ListGroupItem key={id} tag="a" href={`${settings.backendDomain}/orders/files/${order_id}/${val.slug}`} target="_blank">
                                                <FontAwesome name="file-alt"/><span className="file-name-separ">{val.name}</span>
                                            </ListGroupItem>
                                        )
                                    )
                                }
                            </ListGroup>
                        </Col>
                    </Row>
                </Container>
            </div>
        </section>
    );
}