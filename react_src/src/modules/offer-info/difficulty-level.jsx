import React from 'react';
import { Progress } from 'reactstrap';

export default ({value}) => {
    const intVal = parseInt(value);
    const mapping = {
        1: 'B. ŁATWY',
        2: 'ŁATWY',
        3: 'ŚREDNI',
        4: 'TRUDNY',
        5: 'B. TRUDNY',
        99: 'NIE WIEM',
    };
    const calcPerc = ((intVal === 99) ? 0 : ((100*value)/5));
    const colorMap = {
        1: 'primary',
        2: 'info',
        3: 'success',
        4: 'warning',
        5: 'danger',
        99: 'light',
    };

    return (
        <React.Fragment>
            {mapping[value]} <Progress value={calcPerc} color={colorMap[value]}/>
        </React.Fragment>
    )

}
