import React from 'react';
import moment from 'moment';

export default ({to}) => {
    const start = moment();
    const end = moment(to);
    const duration = Math.round(moment.duration(end.diff(start)).asDays());

    if (duration < 0) {
        return (
            <React.Fragment>
                {0}
            </React.Fragment>
        );
    }

    if (duration > 2) {
        return (
            <React.Fragment>
                {to}
            </React.Fragment>
        );
    }

    return (
        <React.Fragment>
            <span className="alert">PILNE</span>{` ${to}`}
        </React.Fragment>
    );
}
