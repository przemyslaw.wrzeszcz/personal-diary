import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import TimeLeft from './time-left';
import DifficultyLevel from './difficulty-level';
import locutusEmpty from "locutus/php/var/empty";
import { MyContext } from "../../context";

const showIndustry = (questions, additional) => {
    return questions.some(function (q) {
        return (additional.hasOwnProperty(`${q.id}-${q.key}`) && !locutusEmpty(additional[`${q.id}-${q.key}`]));
    });
};

class OfferInfo extends React.Component {
    render() {
        const {data} = this.props;
        return (
            <section className="offerInfo">
                <div className="section-background offer-info-window green desktop">
                    <Container fluid>
                        <Row>
                            <Col xs="6">
                                <h3 className="faded">WOJEWÓDZTWO:</h3>
                                <h4>{data.province}</h4>
                            </Col>
                            <Col xs="6">
                                <h3 className="faded">MIEJSCOWOŚĆ:</h3>
                                <h4>{data.city}</h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="9">
                                <h3 className="faded">BRANŻA:</h3>
                                <h4>{data.order_name}</h4>
                            </Col>
                            <Col xs="3">
                                <h3 className="faded">POWIERZCHNIA:</h3>
                                <h4>{data.surface} h</h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="4">
                                <h3 className="faded">TERMIN ROZPOCZĘCIA PRAC:</h3>
                                <h4><TimeLeft to={data.start_date} /></h4>
                            </Col>
                            <Col xs="4">
                                <h3 className="faded">TERMIN ZAKOŃCZENIA PRAC:</h3>
                                <h4>{data.end_date}</h4>
                            </Col>
                            <Col xs="4">
                                <h3 className="faded">STOPIEŃ TRUDNOŚCI:</h3>
                                <h4><DifficultyLevel value={data.difficulty_level} /></h4>
                            </Col>
                        </Row>
                        {
                            (typeof data.additional === 'object') ?
                                this.props.industries.map ((val, id) => {
                                    if (showIndustry(val.questions, data.additional)) {
                                        return (
                                            <React.Fragment key={`${id}-Fragment`}>
                                                <Row key={`${id}-header`} id={`${val.key}-header`}>
                                                    <Col xs="12">
                                                        <h2>{val.name}</h2>
                                                    </Col>
                                                </Row>
                                                <Row key={id} id={val.key}>
                                                    {
                                                        val.questions.map((q, qid) => {
                                                            if (data.additional.hasOwnProperty(`${q.id}-${q.key}`) && !locutusEmpty(data.additional[`${q.id}-${q.key}`])) {
                                                                return (
                                                                    <Col key={qid}>
                                                                        <h3 className="faded">{q.question}</h3>
                                                                        <h4>{data.additional[`${q.id}-${q.key}`]}</h4>
                                                                    </Col>
                                                                )
                                                            }

                                                            return null;
                                                        })
                                                    }
                                                </Row>
                                            </React.Fragment>
                                        )
                                    }
                                })  : null
                        }
                    </Container>
                </div>
                <div className="section-background offer-info-window green mobile">
                    <Container fluid>
                        <Row>
                            <Col xs="6">
                                <h3 className="faded">WOJEWÓDZTWO:</h3>
                                <h4>{data.province}</h4>
                            </Col>
                            <Col xs="6">
                                <h3 className="faded">MIEJSCOWOŚĆ:</h3>
                                <h4>{data.city}</h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="12">
                                <h3 className="faded">BRANŻA:</h3>
                                <h4>{data.order_name}</h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="6">
                                <h3 className="faded">POWIERZCHNIA:</h3>
                                <h4>{data.surface} h</h4>
                            </Col>
                            <Col xs="6">
                                <h3 className="faded">TERMIN ROZPOCZĘCIA PRAC:</h3>
                                <h4><TimeLeft to={data.start_date} /></h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="6">
                                <h3 className="faded">TERMIN ZAKOŃCZENIA PRAC:</h3>
                                <h4>{data.end_date}</h4>
                            </Col>
                            <Col xs="6">
                                <h3 className="faded">STOPIEŃ TRUDNOŚCI:</h3>
                                <h4><DifficultyLevel value={data.difficulty_level} /></h4>
                            </Col>
                        </Row>
                        {
                            (typeof data.additional === 'object') ?
                                this.props.industries.map ((val, id) => {
                                    if (showIndustry(val.questions, data.additional)) {
                                        return (
                                            <React.Fragment key={`${id}-Fragment`}>
                                                <Row key={`${id}-header`} id={`${val.key}-header`}>
                                                    <Col xs="12">
                                                        <h2>{val.name}</h2>
                                                    </Col>
                                                </Row>
                                                <Row key={id} id={val.key}>
                                                    {
                                                        val.questions.map((q, qid) => {
                                                            if (data.additional.hasOwnProperty(`${q.id}-${q.key}`) && !locutusEmpty(data.additional[`${q.id}-${q.key}`])) {
                                                                return (
                                                                    <Col key={qid}>
                                                                        <h3 className="faded">{q.question}</h3>
                                                                        <h4>{data.additional[`${q.id}-${q.key}`]}</h4>
                                                                    </Col>
                                                                )
                                                            }

                                                            return null;
                                                        })
                                                    }
                                                </Row>
                                            </React.Fragment>
                                        )
                                    }
                                })  : null
                        }
                    </Container>
                </div>
                {
                    (data.description.length > 0) ?
                        <div className="offer-description">
                            <Container>
                                <Row>
                                    <Col>
                                        <h3>OPIS:</h3>
                                        <p>{data.description}</p>
                                    </Col>
                                </Row>
                            </Container>
                        </div> : ''
                }

                <hr/>
            </section>
        );
    }
}

export default props => (
    <MyContext.Consumer>
        {({state}) => {
            return <OfferInfo {...props} industries={state.industries} userToken={state.token} />
        }}
    </MyContext.Consumer>
)
