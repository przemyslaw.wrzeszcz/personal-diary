import React from 'react';
import { WithContext as ReactTags } from 'react-tag-input';
import { MyContext } from "../../context";

const KeyCodes = {
    comma: 188,
    enter: 13,
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

export default class Index extends React.Component {
    constructor(props) {
        super(props);

        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
    }

    handleDelete(i) {
        const { tags, setTags } = this.props;
        setTags(tags.filter((tag, index) => index !== i));
    }

    handleAddition(tag) {
        const { tags, setTags } = this.props;
        setTags([...tags, tag]);
    }

    handleDrag(tag, currPos, newPos) {
        const tags = [...this.props.tags];
        const newTags = tags.slice();

        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);

        // re-render
        this.props.setTags(newTags);
    }

    render() {
        const { tags } = this.props;
        return (
            <section className="add-inputs">
                <label>W polu wpisz nazwę maszyny i zatwierdź ją wciskając klawisz "enter".</label>
                <ReactTags tags={tags}
                    handleDelete={this.handleDelete}
                    handleAddition={this.handleAddition}
                    handleDrag={this.handleDrag}
                    classNames={{
                        tagInputField: 'form-control',
                    }}
                    name = "machines"
                    placeholder="Podaj nazwę maszyny"
                    delimiters={delimiters} />
            </section>
        )
    }
}