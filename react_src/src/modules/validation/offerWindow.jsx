import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
    let errors = {};
    if (validator.isEmpty(data['OrderOffersForm[price]'])) {
        errors['OrderOffersForm[price]'] = 'Należy podać ceną';
    }
    if (validator.isEmpty(data['OrderOffersForm[start_date]'])) {
        errors['OrderOffersForm[start_date]'] = 'Należy podać termin rozpoczęcia zlecenia';
    }
    if (validator.isEmpty(data['OrderOffersForm[end_date]'])) {
        errors['OrderOffersForm[end_date]'] = 'Należy podać termin zakończenia zlecenia';
    }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}