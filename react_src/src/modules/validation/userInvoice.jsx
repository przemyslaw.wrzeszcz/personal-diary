import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
import CommonLib from '../../core/common';

export function validateInput(data) {
    let errors = {};

    if (isEmpty(data.company_name) || !isString(data.company_name)) {
        errors.company_name = 'To pole jest wymagane';
    }
    if (isEmpty(data.company_province) || !isString(data.company_province)) {
        errors.company_province = 'To pole jest wymagane';
    }
    if (isEmpty(data.company_city) || !isString(data.company_city)) {
        errors.company_city = 'To pole jest wymagane';
    }
    if (isEmpty(data.company_address) || !isString(data.company_address)) {
        errors.company_address = 'To pole jest wymagane';
    }
    if (!isEmpty(data.nip)) {
        if (!CommonLib.validatenip(data.nip) || !isString(data.nip)) {
            errors.nip = 'Nieprawidłowy numer NIP';
        }
    }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}
