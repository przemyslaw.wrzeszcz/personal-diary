import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
    let errors = {};

    if (validator.isEmpty(data.Iemail)) {
        errors.Iemail = 'To pole jest wymagane';
    }
    if (validator.isEmpty(data.password)) {
        errors.password = 'To pole jest wymagane';
    }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}