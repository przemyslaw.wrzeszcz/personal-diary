import validator from 'validator';
import isEmpty from 'lodash/isEmpty';
import GWCommon from "../../core/common";

export function validateInput(data) {
    let errors = {};

    let startDateNotValid = validator.isEmpty(data['OrdersForm[start_date]']);

    if (startDateNotValid) {
        errors['OrdersForm[start_date]'] = 'Należy podać termin rozpoczęcia prac';
    } else {
        startDateNotValid = (GWCommon.calcDateDiffInDays(data['OrdersForm[start_date]']) < 4);

        if (startDateNotValid) {
            errors['OrdersForm[start_date]'] = 'Termin rozpoczęcia prac musi być oddalony conajmniej 4 dni od dziś';
        }
    }

    let endDateNotValid = validator.isEmpty(data['OrdersForm[end_date]']);

    if (endDateNotValid) {
        errors['OrdersForm[end_date]'] = 'Należy podać termin zakończenia prac';
    } else if (!startDateNotValid) {
        endDateNotValid = (GWCommon.calcDateDiffInDays(data['OrdersForm[end_date]'], data['OrdersForm[start_date]']) < 1);

        if (endDateNotValid) {
            errors['OrdersForm[end_date]'] = 'Termin rozpoczęcia prac musi być oddalony conajmniej 1 dzień od daty rozpoczęcia prac';
        }
    }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}