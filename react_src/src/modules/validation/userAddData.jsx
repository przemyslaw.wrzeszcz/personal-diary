import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
    let errors = {};
    if (validator.isEmpty(data.base_address)) {
        errors.base_address = 'Należy podać adres siedziby';
    }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}