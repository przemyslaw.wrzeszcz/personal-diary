import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
    let errors = {};
    if (validator.isEmpty(data.name)) {
        errors.name = 'Należy podać imię';
    }
    if (validator.isEmpty(data.surname)) {
        errors.surname = 'Należy poodać nazwisko';
    }
    if (validator.isEmpty(data.mail)) {
        errors.mail = 'Należy podać adres email';
    }
    if (validator.isEmpty(data.phone)) {
        errors.phone = 'Należy podać poprawny numer telefonu';
    }

    if (validator.isEmpty(data.rpassword) && validator.isEmpty(data.password)) {
        return {
            errors,
            isValid: isEmpty(errors),
        };
    }

    if (validator.isEmpty(data.old_password)) {
        errors.old_password = 'Pole hasło jest wymagane';
    }
    if (validator.isEmpty(data.password)) {
        errors.password = 'Pole hasło jest wymagane';
    }
    if (validator.isEmpty(data.rpassword)) {
        errors.rpassword = 'Podane hasła się nie zgadzają';
    }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}