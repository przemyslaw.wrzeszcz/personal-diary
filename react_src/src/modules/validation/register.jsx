import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export function validateInput(data) {
    let errors = {};
    if (validator.isEmpty(data.acctype)) {
        errors.acctype = 'Należy wybrać typ klienta';
    }
    if (validator.isEmpty(data.name)) {
        errors.name = 'Należy podać imię';
    }
    if (validator.isEmpty(data.surname)) {
        errors.surname = 'Należy poodać nazwisko';
    }
    if (validator.isEmpty(data.email)) {
        errors.email = 'Należy podać adres email';
    }
    if (Object.keys(errors).length === 0 && data.acctype === 'executor' && validator.isEmpty(data.nip)) {
        errors.nip = 'Należy podać poprawny NIP';
    }
    if (validator.isEmpty(data.phone)) {
        errors.phone = 'Należy podać poprawny numer telefonu';
    }
    if (validator.isEmpty(data.password)) {
        errors.password = 'Pole hasło jest wymagane';
    }
    if (validator.isEmpty(data.rpassword)) {
        errors.rpassword = 'Podane hasła się nie zgadzają';
    }
    if (data.agreement !== true) {
        errors.agreement = 'Wymagane jest wyrażenie zgody';
    }

    return {
        errors,
        isValid: isEmpty(errors),
    }
}