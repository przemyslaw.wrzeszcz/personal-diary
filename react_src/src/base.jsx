import React from 'react';
import CompHeader from './core/header/header';
import CompFooter from './core/footer/footer';

class Base extends React.Component {
    render() {
        return (
            <div>
                <CompHeader/>
                {this.props.children}
                <CompFooter/>
            </div>
        );
    }
}

export default Base;