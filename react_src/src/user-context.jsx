import React from 'react';

export const MyUserContext = React.createContext();
export class MyUserProvider extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            uncompletedAddForm: {},
        };
    }

    render() {
        return (
            <MyUserContext.Provider value={{
                state: this.state,
                setUncompletedAddForm: (data) => {
                    this.setState({
                        uncompletedAddForm: data,
                    });
                },
            }}>
                {this.props.children}
            </MyUserContext.Provider>
        )
    }
}
