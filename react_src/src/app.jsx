import React from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';

import "./core/global/style.scss";
import Base from './base';
import MainPage from './pages/mainpage';
import UserAccount from './pages/user-account';
import SingleOrder from './pages/single-order';
import LoginPage from './pages/login-page';
import RegisterPage from './pages/register-page';
import ErrorPage from './pages/errorsite';
import {MyProvider, MyContext} from './context';
import scrollToComponent from 'react-scroll-to-component';
import axios from "axios/index";
import Preloader from "./modules/preloader/preloader";
import PopupError from "./modules/popup/popup_error";
import settings from './core/settings.js';
import {MyUserProvider} from "./user-context";

Moment.locale('pl');
momentLocalizer();

class MyScroller extends React.Component {

    componentDidMount(){
        if (this.props.headerRef !== null) {
            scrollToComponent(this.props.headerRef, {
                align: 'top',
                duration: 1,
            });
        }
    }

    componentDidUpdate(){
        if (this.props.headerRef !== null) {
            scrollToComponent(this.props.headerRef, {
                align: 'top',
                duration: 1,
            });
        }
    }

    render() {
        return (
            <React.Fragment>
                { this.props.children }
            </React.Fragment>
        )
    }
}

class IndustriesLoader extends React.Component {
    constructor(props) {
        super(props);

        this.runLoader = this.runLoader.bind(this);
    }

    runLoader(){
        if (this.props.userToken.length > 0 && (this.props.industriesLoaded === -1 || this.props.industriesLoaded === -3)) {
            this.props.setIndustries(0, []);

            const getQuestionsUrl = `${settings.backendDomain}/industries?showQuestions=true`;
            const instance = axios.create({
                headers: {'Authorization': `Bearer ${this.props.userToken}`},
            });
            instance.get(getQuestionsUrl, {headers: {'Authorization': `Bearer ${this.props.userToken}`}})
                .then(result => {
                    this.props.setIndustries(1, result.data);
                }).catch((err) => {
                    if (err.response.status === 401) {
                        this.props.setUserAndToken('', {});
                        return this.props.setIndustries(-3, []);
                    }

                    this.props.setIndustries(-2, []);
                });
        }
    }

    componentDidMount(){
        this.runLoader();
    }

    componentDidUpdate(){
        this.runLoader();
    }

    render() {
        return (
            <React.Fragment>
                { (this.props.industriesLoaded === -3) ? (
                    <Redirect to="/logowanie"/>
                ) : ''
                }
                { (this.props.industriesLoaded === -2) ? (
                    <PopupError isOpen={true} title="Wystąpił błąd"><p>Wystąpił błąd aplikacji, należy odświeżyć stronę. Jeśli problem nie ustąpi prosimy o kontakt z administracją</p></PopupError>
                ) : ''
                }
                { (this.props.industriesLoaded === 0) ? (<Preloader />) : '' }
            </React.Fragment>
        )
    }
}

const MyScrollerWrapper = props => (
    <MyContext.Consumer>
        {(context) => {
            return (
                <React.Fragment>
                    <IndustriesLoader setUserAndToken={context.setUserAndToken} setIndustries={context.setIndustries} industriesLoaded={context.state.industriesLoaded} userToken={context.state.token} />
                    <MyScroller {...props} headerRef={context.state.headerRef} />
                </React.Fragment>
            )
        }}
    </MyContext.Consumer>
);


const App = () => (
    <div>
        <MyProvider>
            <BrowserRouter>
                <div id="overAll">
                    <Base>
                        <Route render={({location}) => {
                            return (
                                <MyScrollerWrapper>
                                    <MyUserProvider>
                                        <Switch key={location.key} location={location}>
                                            <Route path="/" exact={true} component={MainPage}/>
                                            <Route path="/zamowienie/:id/:dt" component={SingleOrder}/>
                                            <Route path="/moje-konto" component={UserAccount}/>
                                            <Route path="/logowanie" exact={true} component={LoginPage}/>
                                            <Route path="/rejestracja" exact={true} component={RegisterPage}/>
                                            <Route component={ErrorPage}/>
                                        </Switch>
                                    </MyUserProvider>
                                </MyScrollerWrapper>
                            );
                        }}/>
                    </Base>
                </div>
            </BrowserRouter>
        </MyProvider>
    </div>
);

export default App;