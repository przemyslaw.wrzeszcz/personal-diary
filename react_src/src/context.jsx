import React from 'react';
import locutusEmpty from "locutus/php/var/empty";

export const MyContext = React.createContext();
export class MyProvider extends React.Component {
    constructor(props) {
        super(props);

        this.updateSession = this.updateSession.bind(this);
        const sessionSavedState = localStorage.getItem('abvReactCache');
        const cachedObj = {
            token: '',
            user: {},
        };

        if (sessionSavedState ) {
            const tmp = JSON.parse(sessionSavedState);
            cachedObj.token = tmp.token;
            cachedObj.user = tmp.user;
        }

        this.state = {
            headerRef: null,
            token: cachedObj.token,
            user: cachedObj.user,
            industriesLoaded: -1,
            industries: [],
            sessionMessage: '',
        };
    }

    updateSession() {
        const objToSave = {
            token: this.state.token,
            user: this.state.user,
        };

        localStorage.setItem('abvReactCache', JSON.stringify(objToSave));
    }

    render() {
        return (
            <MyContext.Provider value={{
                state: this.state,
                setHeadRef: (ref) => this.setState({
                    headerRef: ref,
                }),
                setToken: (ref) => {
                    this.setState({
                        token: ref,
                    });
                },
                setSessionMessage: (ref) => {
                    this.setState({
                        sessionMessage: ref,
                    });
                },
                getSessionMessage: () => {
                    const sm = this.state.sessionMessage;

                    if (locutusEmpty(sm) === false) {
                        this.setState({
                            sessionMessage: '',
                        });
                    }

                    return sm;
                },
                setUser: (ref) => {
                    this.setState({
                        user: ref,
                    });

                    this.updateSession();
                },
                setUserAndToken: (token, user) => {
                    this.setState({
                        token: token,
                        user: user,
                    });

                    this.updateSession();
                },
                setIndustries: (loaded, data) => {
                    this.setState({
                        industriesLoaded: loaded,
                        industries: data,
                    });
                },
            }}>
                {this.props.children}
            </MyContext.Provider>
        )
    }
}
