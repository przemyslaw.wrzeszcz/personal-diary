const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    mode: (process.env.NODE_ENV === 'production') ? 'production' : 'development',
    entry: [
        "babel-polyfill",
        path.join(__dirname, '/src/main.jsx'),
    ],
    output: {
        path: path.join(__dirname, './../public/dist'),
        filename: 'bundle.js',
        publicPath: 'dist',
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    /*optimization: {
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true,
                },
            },
        },
    },*/
    plugins: [
        new webpack.LoaderOptionsPlugin({
            options: {
                relativeUrls: true,
            },
        }),
        new MiniCssExtractPlugin({
            filename: 'styles.css',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.jsx?$/, // .js .jsx
                loader: 'babel-loader',
                include: [
                    path.join(__dirname, '/src'),
                ],
            }, {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    // 'resolve-url-loader',
                    "sass-loader",
                ],
            }, {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                ],
            }, {
                test: /\.(jpe?g|png|gif)$/i,
                loader: 'file-loader',
                options: {
                    name: (file) => {
                        if (process.env.NODE_ENV !== 'production') {
                            return '[name].[ext]'
                        }

                        return '[hash].[ext]'
                    },
                    outputPath: 'images',
                    publicPath: 'dist/images',
                },
                include: [
                    path.join(__dirname, '/src'),
                ],
            }, {
                test: /\.(png|jpg)$/,
                loader: 'url-loader',
                options: {
                    name: (file) => {
                        if (process.env.NODE_ENV !== 'production') {
                            return '[name].[ext]'
                        }

                        return '[hash].[ext]'
                    },
                    outputPath: 'images',
                    publicPath: 'dist/images',
                },
                include: [
                    path.join(__dirname, '/node_modules'),
                ],
            }, {
                test: /\.(woff|woff2|eot|ttf|svg)$/i,
                loader: 'file-loader',
                options: {
                    name: (file) => {
                        if (process.env.NODE_ENV !== 'production') {
                            return '[name].[ext]'
                        }

                        return '[hash].[ext]'
                    },
                    outputPath: 'fonts',
                    publicPath: '/dist/fonts',
                },
                include: [
                    path.join(__dirname, '/src'),
                ],
            },
        ],
    },
    devServer: {
        contentBase: path.join(__dirname, "public"),
        compress: true,
        port: 8000,
        host: '0.0.0.0',
        disableHostCheck: true,
        hot: false,
        historyApiFallback: true,
    },
};